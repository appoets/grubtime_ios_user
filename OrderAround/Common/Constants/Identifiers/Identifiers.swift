//
//  Identifiers.swift
//  User
//
//  Created by imac on 12/19/17.
//  Copyright © 2017 Appoets. All rights reserved.
//

import Foundation

// MARK:- Storyboard Id
struct Storyboard {
    
    static let Ids = Storyboard()
    
    let RootViewController = "RootViewController"
    let SignSkipViewController = "SignSkipViewController"
    let SignInViewController = "SignInViewController"
    let MobileViewController = "MobileViewController"
    let ChangePasswordController = "ChangePasswordController"
    let SignUpViewController = "SignUpViewController"
    let CountryCodeController = "CountryCodeController"
    let VerificationViewController = "VerificationViewController"
    let SocialLoginViewController = "SocialLoginViewController"
    let BaseTabController = "BaseTabController"
    let HomeViewController = "HomeViewController"
    let SearchForResturantDishesController = "SearchForResturantDishesController"
    let CartViewController = "CartViewController"
    let EditProfileViewController = "EditProfileViewController"
    let ForgotPasswordController = "ForgotPasswordController"
    let PromoCodeViewController = "PromoCodeViewController"
    let FavoritesViewController = "FavoritesViewController"
    let AddNewCardViewController = "AddNewCardViewController"
    let AddAmountViewController = "AddAmountViewController"
    let PamentListViewController = "PamentListViewController"
    let WalletListViewController = "WalletListViewController"
    let UserAccountViewController = "UserAccountViewController"
    let DeliveryLocationViewController = "DeliveryLocationViewController"
    let ResturantMenuListViewController = "ResturantMenuListViewController"
    let AddOnsViewController = "AddOnsViewController"
    let ManageAddressViewController = "ManageAddressViewController"
    let MyOrdersViewController = "MyOrdersViewController"
    let FilterViewController = "FilterViewController"
    let SaveDeliveryLocationViewController = "SaveDeliveryLocationViewController"
    let HelpViewController = "HelpViewController"
    let OrderTrackingViewController = "OrderTrackingViewController"
    let ChatViewConroller = "ChatViewController"
    let ChangeLanguageViewController = "ChangeLanguageViewController"
    let ChooseLoginTypeViewController = "ChooseLoginTypeViewController"
    let SetLocationViewController = "SetLocationViewController"
    let CustomNotesViewController = "CustomNotesViewController"
    let FeedBackController = "FeedBackController"
    let ReferralViewController = "ReferralViewController"
    let Nav = "Nav"
    let TermsViewController = "TermsViewController"
    let RefundHistoryViewController = "RefundHistoryViewController"
    let CusinesSuggestionViewController = "CusinesSuggestionViewController"
}


//MARK:- XIB Cell Names

struct XIB {
    
    static let Names = XIB()
    
    let CountryCodeCell = "CountryCodeCell"
    let TrendingCell = "TrendingCell"
    let FavCuisineCollectionViewCell = "FavCuisineCollectionViewCell"
    let OrderListCell = "OrderListCell"
    let CollectionTableCell = "CollectionTableCell"
    let FavouriteCollectionTableViewCell = "FavouriteCollectionTableViewCell"
    let DishesListCell = "DishesListCell"
    let CartListView = "CartListView"
    let CartResturant = "CartResturant"
    let CartCheckOut = "CartCheckOut"
    let UserMenu = "UserMenu"
    let PromoCode = "PromoCode"
    let AddWalletAmount = "AddWalletAmount"
    let WalletHistory = "WalletHistory"
    let AccountDetails = "AccountDetails"
    let LogoutCell = "LogoutCell"
    let SavedLocation = "SavedLocation"
    let AddCartCell = "AddCartCell"
    let AddCartWithImgView = "AddCartWithImgView"
    let ResturantRatingCell = "ResturantRatingCell"
    let ItemDescriptions = "ItemDescriptions"
    let IngradientsCell = "IngradientsCell"
    let CustomNotesCell = "CustomNotesCell"
    let AddOnsCell = "AddOnsCell"
    let ItemImageCell = "ItemImageCell"
    let AddressManageCell = "AddressManageCell"
    let AddNewAddressCell = "AddNewAddressCell"
    let MyOrdersCell = "MyOrdersCell"
    let FilterCell = "FilterCell"
    let SaveDeliveryLocationCell = "SaveDeliveryLocationCell"
    let AddressAddingCell = "AddressAddingCell"
    let OrderTrackCell = "OrderTrackCell"
    let HelpCell = "HelpCell"
    let OrderDetailsCell = "OrderDetailsCell"
    let OrderPaymentCell = "OrderPaymentCell"
    let FeedBackViewController = "FeedBackViewController"
    let SectionHeaderCell = "SectionHeaderCell"
    let GoBackCell = "GoBackCell"
    let CustomNotesViewController = "CustomNotesViewController"
    let PageViewXib = "OnBoardPageViewControl"
    let PageViewCellXib = "OnBoardPageViewCell"
    let CancelView = "CancelView"
    let CartPromoCodeTableViewCell = "CartPromoCodeTableViewCell"
    let FavortieFoodCollectionViewCell = "FavortieFoodCollectionViewCell"
    let FavouriteFood = "FavouriteFood"
    let SelectPickUpType = "SelectPickupView"
    let CategoryMenu = "CategoryMenu"
    let CategoryCell = "CategoryCell"
    let RefundHistoryTableViewCell = "RefundHistoryTableViewCell"
    let KtchenTitleTableViewCell = "KtchenTitleTableViewCell"
    

   
}

//MARK: - Struct for customViews

struct ViewXib {
 
    static let ids = ViewXib()
    let ImageOnZoomViewController = "ImageOnZoomViewController"
     let PageControlCell = "PageControlCell"
    
    
}


//MARK:- Notification

extension Notification.Name {
   //public static let reachabilityChanged = Notification.Name("reachabilityChanged")
}



