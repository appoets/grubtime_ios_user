//
//  Utility.swift
//  orderAround
//
//  Created by Prem's on 28/08/20.
//  Copyright © 2020 css. All rights reserved.
//

import Foundation
import UIKit

public enum DisplayType {
    case unknown
    case iphone4
    case iphone5
    case iphone6
    case iphone6plus
    case iphoneX
    case iphoneXMAX
    static let iphone7 = iphone6
    static let iphone7plus = iphone6plus
    static let iphone8 = iphone6
    static let iphone8plus = iphone6plus
    static let iphoneXR = iphoneXMAX
    static let iphoneXS = iphoneX
}

public final class Display {
    class var width:CGFloat { return UIScreen.main.bounds.size.width }
    class var height:CGFloat { return UIScreen.main.bounds.size.height }
    class var maxLength:CGFloat { return max(width, height) }
    class var minLength:CGFloat { return min(width, height) }
    class var zoomed:Bool { return UIScreen.main.nativeScale >= UIScreen.main.scale }
    class var retina:Bool { return UIScreen.main.scale >= 2.0 }
    class var phone:Bool { return UIDevice.current.userInterfaceIdiom == .phone }
    class var pad:Bool { return UIDevice.current.userInterfaceIdiom == .pad }
    class var carplay:Bool { return UIDevice.current.userInterfaceIdiom == .carPlay }
    class var tv:Bool { return UIDevice.current.userInterfaceIdiom == .tv }
    class var typeIsLike:DisplayType {
        if phone && maxLength < 568 {
            return .iphone4
        }
        else if phone && maxLength == 568 {
            return .iphone5
        }
        else if phone && maxLength == 667 {
            return .iphone6
        }
        else if phone && maxLength == 736 {
            return .iphone6plus
        }
        else if phone && maxLength == 812 {
            return .iphoneX
        }
        else if phone && maxLength == 896 {
            return .iphoneXMAX
        }
        return .iphone6plus//.unknown
    }
}

class Utility{
    
    static let instance = Utility()
    
    func topViewControllerWithRootViewController(rootViewController: UIViewController) -> UIViewController {
        if let tabbarController = rootViewController as? UITabBarController, let selectedViewController = tabbarController.selectedViewController{
            return self.topViewControllerWithRootViewController(rootViewController: selectedViewController)
        } else if
            let navigationController = rootViewController as? UINavigationController,
            let visibleViewController = navigationController.visibleViewController {
            return self.topViewControllerWithRootViewController(rootViewController: visibleViewController)
        } else if let presentedController = rootViewController.presentedViewController {
            return self.topViewControllerWithRootViewController(rootViewController: presentedController)
        } else {
            return rootViewController
        }
    }
    
    func setupHeaderViewHeight() -> CGFloat {
        if Display.typeIsLike == .iphoneX || Display.typeIsLike == .iphoneXMAX || Display.typeIsLike == .iphoneXS || Display.typeIsLike == .iphoneXR {
            return 100
        }else{
            return 64
        }
    }
}
