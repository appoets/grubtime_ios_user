//
//  ReferralViewController.swift
//  orderAround
//
//  Created by Chan Basha on 21/01/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class ReferralViewController: UIViewController {

    @IBOutlet weak var labelReferral: UILabel!
    
    @IBOutlet weak var labelReferralCode: UILabel!
    
    @IBOutlet weak var labelContent: UILabel!
    
    @IBOutlet weak var btnCopy: UIButton!
    
    @IBOutlet weak var btnInviteReferral: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
       initialLoads()
    
        
        
    }
    
    
    @IBAction func copy(sender:UIButton){
        
      UIPasteboard.general.string = User.main.referral_code
    
     self.view.make(toast: "Copied")
        
        
    }
    
 
    private func initialLoads(){
        
        Common.setFont(to: labelReferral, isTitle: true, size: 20, fontType: .bold)
        Common.setFont(to: labelContent, isTitle: false, size: 12, fontType: .semiBold)
        Common.setFont(to: labelReferralCode, isTitle: true, size: 15, fontType: .bold)
        Common.setFont(to: btnInviteReferral, isTitle: true, size: 17, fontType: .bold)

        self.labelReferralCode.text = User.main.referral_code
        self.btnInviteReferral.addTarget(self, action: #selector(shareAction(sender:)), for: .touchUpInside)
        self.btnCopy.addTarget(self, action: #selector(copy(sender:)), for: .touchUpInside)

        
        
    }
    
    @IBAction func shareAction(sender:UIButton){
        
        
        let shareItems:Array = [#imageLiteral(resourceName: "user_logo"),"Referral Code : \(User.main.referral_code ?? "")"] as [Any]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }
   
    
    @IBAction func backAction(_ sender: UIButton) {
        
        
        
        
        self.navigationController?.popViewController(animated: true)

        
        
        
    }
    
    
}
