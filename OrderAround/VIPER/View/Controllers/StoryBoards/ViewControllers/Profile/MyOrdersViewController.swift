//
//  ManageAddressViewController.swift
//  Project
//
//  Created by CSS on 23/10/18.
//  Copyright © 2018 css. All rights reserved.
//

import UIKit

class MyOrdersViewController: UIViewController {
    
    @IBOutlet weak var shimmerView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    var headerHeight: CGFloat = 55
      
    @IBOutlet weak var tableView: UITableView!
//    var  orderDataSource = [String:[OrderList]]()
    var ongoingOrderSource = [OrderList]()
    var pastOrderSource = [OrderList]()
    var refreshControl = UIRefreshControl()
    private lazy var  loader = {
           return createActivityIndicator(UIApplication.shared.keyWindow ?? self.view)
       }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shimmerView.isHidden = false
        localize()
        tableView.register(UINib(nibName: XIB.Names.MyOrdersCell, bundle: nil), forCellReuseIdentifier: XIB.Names.MyOrdersCell)
     
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.get(api: .onGoingOrders, data: nil)
        DispatchQueue.main.async {
         self.presenter?.get(api: .pastOrders, data: nil)
                    
                         }
        addRefreshControl()

    }
    func addRefreshControl() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
    }
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        self.presenter?.get(api: .onGoingOrders, data: nil)
        DispatchQueue.main.async {
            self.presenter?.get(api: .pastOrders, data: nil)
                        
                             }
    }
    
    private func localize() {
        labelTitle.text = APPLocalize.localizestring.order.localize()
        Common.setFont(to: labelTitle, isTitle: true, size: 15, fontType: .semiBold)
    }
    
    @IBAction func backToPrevious(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension MyOrdersViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
       // return onGoing.count > 0 ? 1 : 0//self.dataSource.count > 0 ? dataSource.count : 0
        return 2 //orderDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        guard let data = orderDataSource  else { return 0 }
      //  return Array(orderDataSource.values)[section].count
        
        switch section {
        case 0:
            return ongoingOrderSource.count
        case 1:
            return pastOrderSource.count
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190 //UITableViewAutomaticDimension
    }
    
    func  tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

//        return self.tableView.headerView(height: headerHeight, text:Array(orderDataSource.keys)[section])
        
        switch section {
        case 0:
            return self.tableView.headerView(height: headerHeight, text: APPLocalize.localizestring.currentOrders.localize().uppercased())
        case 1:
            return self.tableView.headerView(height: headerHeight, text: APPLocalize.localizestring.pastOrders.localize().uppercased())
        default:
            return self.tableView.headerView(height: headerHeight, text: "")

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.MyOrdersCell, for: indexPath) as! MyOrdersCell
//        print(orderDataSource.values.count)
        print(indexPath.row)
        cell.delegate = self
//        if Array(orderDataSource.keys)[indexPath.section] == APPLocalize.localizestring.currentOrders.localize() {
//            cell.setValues(values: Array(orderDataSource.values)[indexPath.section][indexPath.row])
//        }else{
//            cell.setValues(values: Array(orderDataSource.values)[indexPath.section][indexPath.row])
//        }
        switch indexPath.section {
        case 0:
            cell.setValues(values: ongoingOrderSource[indexPath.row])
            
            
        case 1:
            cell.setValues(values: pastOrderSource[indexPath.row])
            
        default:
            return UITableViewCell()
        }
        
//        if Array(orderDataSource.keys)[indexPath.section] == APPLocalize.localizestring.currentOrders.localize() {
//            cell.setValues(values: Array(orderDataSource.values)[indexPath.section][indexPath.row])
//        }else{
//            cell.setValues(values: Array(orderDataSource.values)[indexPath.section][indexPath.row])
//        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0){
//            let orderType = ongoingOrderSource[indexPath.row]
            let vc = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.OrderTrackingViewController) as! OrderTrackingViewController
            vc.isPastOrder =  false
            vc.isFromMyOrder = true
            vc.orderDetails = ongoingOrderSource[indexPath.row]
            vc.isfromMyOrder = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
//            let orderType = ongoingOrderSource[indexPath.row]
                       let vc = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.OrderTrackingViewController) as! OrderTrackingViewController
                       vc.isPastOrder =  true
                       vc.isFromMyOrder = true
                       vc.orderDetails = pastOrderSource[indexPath.row]
                       vc.isfromMyOrder = true
                       self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

extension MyOrdersViewController : PostViewProtocol {
    func onError(api: Base, message: String, statusCode code: Int) {
        refreshControl.endRefreshing()
        Common.showToast(string: message)
        self.shimmerView.isHidden = true
    }
    
    func getOnGoingOrder(api: Base, data: [OrderList]?) {
        refreshControl.endRefreshing()
        if api == .onGoingOrders {
            if data?.count ?? 0 > 0 {
//                orderDataSource[APPLocalize.localizestring.currentOrders.localize().uppercased()] = data ?? []
                ongoingOrderSource = data ?? []
            }
            print("Ongoing \(ongoingOrderSource)")
                     
            //self.loader.isHidden  = false
            self.loader.isHidden  = true
            setUpTable()
        }else if api == .pastOrders{
            if data?.count ?? 0 > 0 {
                pastOrderSource = data ?? []
//                orderDataSource[APPLocalize.localizestring.pastOrders.localize().uppercased()] = data ?? []
            }
            print("Past \(pastOrderSource.count)")
            self.loader.isHidden = true
            setUpTable()
        }
//        setUpTable()
    }
    
    func setUpTable() {
        self.shimmerView.isHidden = true
        
        let arr = ongoingOrderSource + pastOrderSource
        guard arr.count > 0 else {
            self.tableView.setBackgroundImageAndTitle(imageName: EmptyImage.orderEmpty.rawValue, title: APPLocalize.localizestring.orderEmptyHead.localize(), description: APPLocalize.localizestring.orderEmptyContent.localize())
            return
        }
        self.tableView.reloadInMainThread()
    }
}

extension MyOrdersViewController: MyOrderDetailDelegate{
    func pushtocartViewController(data: OrderList) {
        /*if data.shop_id != User.main.cart?.first?.id {//User.main.cart?.first?.product?.shop_id {
         showAlert(message: APPLocalize.localizestring.anotherRestaurant.localize(), okHandler: {
         self.reorderAPI(data: data)
         }, fromView: self)
         }else if data.id == User.main.cart?.first?.id {
         showAlert(message: APPLocalize.localizestring.anotherDishesSameRestaurant.localize(), okHandler: {
         self.reorderAPI(data: data)
         }, fromView:  self)
         }else{
         self.reorderAPI(data: data)
         }*/
        
        if User.main.cartCount ?? 0 > 0{
            if data.shop_id != User.main.cart?.first?.product?.shop_id ?? 0 {
                let cartAlert = UIAlertController(title: APPLocalize.localizestring.replaceCartItem.localize(), message: APPLocalize.localizestring.replaceCart.localize(), preferredStyle: .alert)
                
                cartAlert.addAction(UIAlertAction(title: APPLocalize.localizestring.yes.localize(), style: .default, handler: { (Void) in
                    self.reorderAPI(data: data)
                }))
                
                cartAlert.addAction(UIAlertAction(title: APPLocalize.localizestring.no.localize(), style: .default, handler: nil))
                self.present(cartAlert, animated: true, completion: nil)
            }else if data.shop_id == User.main.cart?.first?.product?.shop_id ?? 0{
                
                let cartAlert = UIAlertController(title: APPLocalize.localizestring.replaceCartItem.localize(), message: APPLocalize.localizestring.anotherDishesSameRestaurant.localize(), preferredStyle: .alert)
                
                cartAlert.addAction(UIAlertAction(title: APPLocalize.localizestring.yes.localize(), style: .default, handler: { (Void) in
                    self.reorderAPI(data: data)
                }))
                
                cartAlert.addAction(UIAlertAction(title: APPLocalize.localizestring.no.localize(), style: .default, handler: nil))
                self.present(cartAlert, animated: true, completion: nil)
            }else {
                self.reorderAPI(data: data)
            }
        }else{
            self.reorderAPI(data: data)
        }
    }
    
    func reorderAPI(data: OrderList) {
        var reorder = Reorder()
        reorder.order_id = data.id
        self.presenter?.post(api: .reOrder, data: reorder.toData())
    }
}

extension MyOrdersViewController {
    
    func getReorder(api: Base, data: Carts?) {
        let cartVC = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.CartViewController) as! CartViewController
        cartVC.isFromResturantCart = true
        
        self.navigationController?.pushViewController(cartVC, animated: true)
    }
}


