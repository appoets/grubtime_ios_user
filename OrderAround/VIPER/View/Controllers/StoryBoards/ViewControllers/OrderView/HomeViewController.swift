//
//  HomeViewController.swift
//  Project
//
//  Created by CSS on 15/10/18.
//  Copyright © 2018 css. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation
import GoogleMaps
import GooglePlaces
import Alamofire
import Crashlytics
import FaveButton
import HSPopupMenu

struct SHOPSTATUS {
    
    let closed = "CLOSED"
    let open = "OPEN"
}
class HomeViewController: UIViewController {
    
    //MARK: - Declarations.
    @IBOutlet weak var noResturantFoundLbl: UILabel!
    @IBOutlet weak var seeIfAnyOtherFilterLbl: UILabel!
    @IBOutlet weak var filterLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    
    @IBOutlet var locationBtn: UIButton!
    @IBOutlet weak var shimmerView: UIView!
    
    @IBOutlet weak var filterImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imageDownArrow: UIImageView!
    @IBOutlet weak var imageFilter: UIImageView!
    @IBOutlet weak var filterButton: UIButton!
    
    @IBOutlet weak var filterView: UIView!
    
    
    @IBOutlet weak var mainView: UIView!
    var FavouriteFood : FavouriteFood?
    private var blurView : UIView?
    private lazy var  loader = {
        return createActivityIndicator(UIApplication.shared.keyWindow ?? self.view)
    }()
    
    @IBOutlet weak var buttonFav: FaveButton!
    
    private var  headerHeight: CGFloat = 30
    var locationManager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D()
    var userLocation: UserCurrentLocation?
    var shopList =  [Shops]()
    var favCuisineShops = [Shops]()
    var freeDeliverShops = [Shops]()
    var kitchens = [Shops]()
    var bannerImg = [Banners]()
    var refreshControl = UIRefreshControl()
    var filterDetailsArray = [String:[FilterItems]]()
    var partyCateringShops = [Shops]()
    var mealServiceShops = [Shops]()
    
    var bgImg : UIImageView?
    var selectedFoodItem  = [Int]()
    var cusinesList = [Cuisines]()
    var isCuisinesShown = false
    var favcollectionView: UICollectionView!
    var FreeDeliverycollectionView: UICollectionView!
    
    let layout = UICollectionViewFlowLayout()
    
    var resturantFilterArray: [ResturantFilter] = []
    var filterViewIsApplied: Bool = false
    var sortArray: [String] = ["Delivery Type", "Rating", "Pickup"]
    var selectedSortIndex: Int?
    var suggestedCusines: [Cuisines] = []
    
    var restitle : String? {
        didSet {
            restitle = shopList.count > 0 ? APPLocalize.localizestring.totalResturant.localize() : ""
        }
    }
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        for family: String in UIFont.familyNames
        {
            print(family)
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
        
        //foodCollection()
        settingServiceCall()
        controllerBasicSetup()
        addRefreshControl()
        // Register to receive notification
        // forceLogout()
        favcollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        FreeDeliverycollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .vertical
        buttonFav.isUserInteractionEnabled = appDelegate.isSkip ? false : true
        self.buttonFav.addTarget(self, action: #selector(showFav(sender:)), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(true)

        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false


        self.presenter?.get(api: .addCart, data: nil)

        if isAppliedFilter(){
            filterImageView.isHidden = false
            self.restaurantFilter()

        } else {
            filterImageView.isHidden = true
            self.restaurantFilter()
        }

        DispatchQueue.main.async {
            self.getProfile()
        }


        if  User.main.cart != nil {
            if let tabItems = tabBarController?.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[2]
                if User.main.cartCount == 0 || User.main.cartCount == nil {
                    tabItem.badgeValue = nil
                }else{
                    tabItem.badgeValue = "\(User.main.cartCount ?? 0)"
                }
            }
        }


        self.loader.isHidden = true

        //  foodCollection()
        //  self.view.addSubview(FavouriteFood)

    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.loader.isHidden = true
    }
    
    @IBAction func showFav(sender:UIButton){
        
        self.showBgView(hidden: false)
        self.isCuisinesShown = false
        
        self.view.addBlurview {
            self.foodCollection(cusines: self.cusinesList)
        }
        
        
    }
    
    private func getFavCuisines(){
        
        
        
        
    }
    
    
    func foodCollection(cusines:[Cuisines])
    {
        
        if self.FavouriteFood == nil, let FavouriteFood = Bundle.main.loadNibNamed(XIB.Names.FavouriteFood, owner: self, options: [:])?.first as? FavouriteFood {
            self.showBgView(hidden: false)
            self.FavouriteFood = FavouriteFood
            self.FavouriteFood?.clipsToBounds = true
            // self.FavouriteFood?.frame = CGRect(x: 10, y: (self.view.frame.height - 450)/2, width: self.view.frame.width * 0.85, height: self.view.frame.height * 0.6)
            self.FavouriteFood?.center = (view.superview?.convert(view.center, to: self.FavouriteFood?.superview))!
            self.FavouriteFood?.layer.cornerRadius = 10.0
            self.FavouriteFood?.data = cusines
            //self.FavouriteFood?.setupSelections(list: cusines)
            self.view.addSubview(FavouriteFood)
            self.isCuisinesShown = true
            mainView.blurView.alpha = 1
            filterView.blurView.alpha = 1
            
        }
        
        self.FavouriteFood?.onClickButton = { food in
            
            print("food>>>",food)
            
            self.selectedFoodItem = food
            
            var idValue = [String : Int]()
            
            var dic = [String:Any]()
            //var count = 0
            
            var cusine = [Int]()
            
            for index in 0..<self.selectedFoodItem.count{
                
                // dic.setValue("\(self.selectedFoodItem[index])", forKey: "cuisines[\(index)]")
                // idValue.updateValue(self.selectedFoodItem[index], forKey:  "cuisines[\(index)]")
                
                cusine.append(self.selectedFoodItem[index])
            }
            
            // let cuisinesData: Data = NSKeyedArchiver.archivedData(withRootObject: dic)
            
            dic["favourite"] = cusine
            
            let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
            DispatchQueue.main.async {
                
                //self.loader.isHidden = false
                self.loader.isHidden = true
                self.presenter?.post(api: .userProfile, data:jsonData)
                
            }
            
            // self.updateCuisines(params: idValue)
            self.FavouriteFood?.removeFromSuperview()
            self.FavouriteFood = nil
            self.showBgView(hidden: true)
            self.view.removeBlurView()
            self.getFavCuisines()
            //self.buttonFav.setImage(self.selectedFoodItem.count > 0 ? UIImage(named: "fav-selected") : UIImage(named: "fav-unselected"), for: .normal)
            self.buttonFav.setSelected(selected: self.selectedFoodItem.count > 0 ? true : false, animated: false)
        }
    }
    
    
    func updateCuisines(params:[String:Int]?)  {
        
        let headers = ["Authorization" : "Bearer "+User.main.accessToken!+"",
                       "Content-Type": "application/json"]
        
        Alamofire.request(URL(string:"https://grubtime.me/api/user/profile")!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            switch response.result
            {
            case .success(let json):
                let jsonData = json as! Any
                print(jsonData)
            case .failure(let error):
                //  self.errorFailer(error: error)
                print(error)
            }
        }
        
        
    }
    
    
    private func addBlurView() {
        
        self.blurView = UIView(frame: UIScreen.main.bounds)
        self.blurView?.alpha = 0
        self.blurView?.backgroundColor = .black
        self.blurView?.isUserInteractionEnabled = true
        self.view.addSubview(self.blurView!)
        //  self.blurView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.hideRecipt)))
        UIView.animate(withDuration: 0.2, animations: {
            self.blurView?.alpha = 0.6
        })
        
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.CollectionTableCell) as? CollectionTableCell
        cell?.collectionView.collectionViewLayout.invalidateLayout()
        
    }
    
    
    
    
    //    func getProfile()
    //    {
    //
    //        var dic = [String:Any]()
    //              dic["device_id"] =  UUID().uuidString
    //               dic["device_token"] = deviceTokenString
    //              dic["device_type"] = "ios"
    //
    //             // self.presenter?.get(api: .userProfile, data: nil)
    //
    //              let jsonData = try? JSONSerialization.data(withJSONObject: dic, options: [])
    //                         DispatchQueue.main.async {
    //
    //                             self.loader.isHidden = false
    //                             self.presenter?.get(api: .userProfile, data:jsonData)
    //
    //                  }
    //
    //         self.loader.isHidden = true
    //
    //    }
    
    func getProfile(){
        
        
        let deviceID = UUID().uuidString
        let url = "\(Base.userProfile.rawValue)" + "?device_id=" + deviceID + "&device_token=" + deviceTokenString + "&device_type=ios"
        self.presenter?.get(api: .userProfile, url:url)
        
        
    }
    
    func settingServiceCall() {
        self.presenter?.get(api: .settings, data: nil)
    }
    
    func addRefreshControl() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
    }
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        getUserProfileDetails()
    }
    
    private func taleViewSetup() {
        
        tableView.register(UINib(nibName: XIB.Names.CollectionTableCell, bundle: nil), forCellReuseIdentifier: XIB.Names.CollectionTableCell)
        tableView.register(UINib(nibName: XIB.Names.OrderListCell, bundle: nil), forCellReuseIdentifier: XIB.Names.OrderListCell)
        tableView.register(UINib(nibName: XIB.Names.FavouriteCollectionTableViewCell, bundle: nil), forCellReuseIdentifier: XIB.Names.FavouriteCollectionTableViewCell)
        tableView.register(HomeSortTableViewCell.nib, forCellReuseIdentifier: HomeSortTableViewCell.cellIdentifier)
        tableView.separatorStyle = .none
    }
    
    private func controllerBasicSetup()
    {
        
        locatingCurrentLocation()
        localize()
        setCustomFont()
        taleViewSetup()
        addObservers()
        self.imageDownArrow.imageTintColor(color1: .secondary)
        self.imageFilter.imageTintColor(color1: .secondary)
        self.locationBtn.isUserInteractionEnabled = false
        
    }
    
    private func addObservers() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    func locatingCurrentLocation() {
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    
    //    @IBAction func FilterRestaurantAction(_ sender: UIButton) {
    //        guard let filterData = filterDetailsArray else { return }
    //        let filter = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.FilterViewController) as! FilterViewController
    //
    //
    //                if filterApplied() {
    //
    //
    //                     filter.filterDataSource = getFilterDetails()
    //                } else {
    //
    //                }
    //
    //        filter.filterDataSource = filterData
    //        //self.present(filter, animated: true, completion: nil)
    //        self.navigationController?.pushViewController(filter, animated: true)
    //    }
    
    
    @IBAction func FilterRestaurantAction(_ sender: UIButton) {
        let filter = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.FilterViewController) as! FilterViewController
        filter.delegate = self
        filterViewIsApplied = false
        if filterApplied() {//resturantFilterApplied()
            filter.filterDataSource = getFilterDetails()
            filter.resturantFilterArray = getResturantFilterDetails()
        } else {
            guard filterDetailsArray.count != 0 else { return }
            filter.filterDataSource = filterDetailsArray
            filter.resturantFilterArray = self.resturantFilterArray
        }
        
        //        filter.filterDataSource = filterData
        // let navigationController = UINavigationController(rootViewController: filter)
        filter.navigationController?.navigationBar.isHidden = true
        //            filter.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(filter, animated: true)
    }
    
}

extension HomeViewController: FilterViewDelegate{
    
    func getFilterStatus(isApplied: Bool) {
        
        filterViewIsApplied = isApplied
    }
}


//MARK: - Button Actions.
extension HomeViewController {
    
    @IBAction func gettingCurrentAddress(_ sender: UIButton) {
        if User.main.id != nil {
            let locationVC = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.DeliveryLocationViewController) as! DeliveryLocationViewController
            locationVC.isFromHome = true
            locationVC.delegate =  self
            self.navigationController?.pushViewController(locationVC, animated: true)
        } else {
            let signIn = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.SignInViewController) as! SignInViewController
            self.navigationController?.pushViewController(signIn, animated: true)
        }
        
    }
    
    func filterApplied() -> Bool {
        
        if getFilterDetails().count > 0 {
            let filters = getFilterDetails().map({$0.value.filter({$0.state == true})}).flatMap({$0})
            if filters.count > 0 {
                return true
            } else {
                return false
            }
            
        }else {
            return false
        }
    }
    
    func resturantFilterApplied() -> Bool {
        
        if getResturantFilterDetails().count > 0{
            let filters = getResturantFilterDetails().map{$0.items.filter{$0.state == true}}.flatMap{$0}
            if filters.count > 0 {
                return true
            } else {
                return false
            }
            
        }else {
            return false
        }
    }
    
    //MARK:- Show Custom Toast
    private func showToast(string : String?) {
        self.view.makeToast(string, point: CGPoint(x: UIScreen.main.bounds.width/2 , y: UIScreen.main.bounds.height/2), title: nil, image: nil, completion: nil)
    }
}

//MARK: - StringLocalize & Font design
extension HomeViewController {
    
    func localize() {
        filterLbl.text = APPLocalize.localizestring.filters.localize()
        locationLbl.text = "Delivering To"//APPLocalize.localizestring.Delivering.localize()
    }
    
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .wifi:
            print("Wifi")
        case .cellular:
            print("Cellular")
        case .none:
            print("none")
        }
    }
    func setCustomFont() {
        
        Common.setFont(to: filterLbl, isTitle: true, size: 12, fontType: .semiBold)
        Common.setFont(to: locationLbl, isTitle: true, size: 15, fontType: .bold)
        Common.setFont(to: addressLbl, isTitle: true, size: 14, fontType: .light)
        Common.setFont(to: noResturantFoundLbl, isTitle: true, size: 16, fontType: .semiBold)
        Common.setFont(to: seeIfAnyOtherFilterLbl, isTitle: true, size: 14, fontType: .semiBold)
        
    }
}

//MARK: TableViewDelegate & TableViewDataSource

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return 1
        case 4:
            return 1
        case 5:
            return self.suggestedCusines.count > 0 ? 1 : 0
        case 6:
            return self.kitchens.count > 0 ? 1 : 0
        case 7:
            return self.kitchens.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60)
        headerView.backgroundColor = .white
        let headerLbl = UILabel()
        headerLbl.frame = CGRect(x: 15, y: 0, width: tableView.frame.width - (2 * 20), height: 30)
        let subTitleLbl = UILabel()
        subTitleLbl.frame = CGRect(x: 15, y: 30, width: tableView.frame.width - (2 * 20), height: 30)
        Common.setFont(to: headerLbl, size : 20, fontType : FontCustom.SuezOneRegular)
        Common.setFont(to: subTitleLbl, size : 15, fontType : FontCustom.regular)
        headerLbl.textAlignment = .left
        subTitleLbl.textAlignment = .left
        headerView.addSubview(headerLbl)
        headerView.addSubview(subTitleLbl)
        headerLbl.textColor = .black
        subTitleLbl.textColor = .systemGray
        
        switch section {
        case 0:
            headerLbl.text = "Top offers for you" //APPLocalize.localizestring.letsImpress.localize()
            return headerView
        case 1:
            headerLbl.text = APPLocalize.localizestring.yourFavourites.localize()
            return headerView
        case 2:
            headerLbl.text = APPLocalize.localizestring.freeDelivery.localize()
            return headerView
        case 3:
            headerLbl.text = "Party Catering"
            return headerView
        case 4:
            headerLbl.text = "Meal Service"
            return headerView
        case 5:
            headerLbl.text = "Popular Cusines Around You"
            return headerView
        case 6:
            return UIView()
        case 7:
            headerLbl.text = APPLocalize.localizestring.totalResturant.localize()
            subTitleLbl.text = "Discover unique kitchens nearby"
            return headerView
        default:
            return UIView()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch section {
        case 0:
            return bannerImg.count > 0 ? headerHeight : 0
        case 1:
            return favCuisineShops.count > 0 ? headerHeight : 0
        case 2:
            return freeDeliverShops.count > 0 ? headerHeight: 0
        case 3:
            return partyCateringShops.count > 0 ? headerHeight : 0
        case 4:
            return mealServiceShops.count > 0 ? headerHeight : 0
        case 5:
            return suggestedCusines.count > 0 ? headerHeight : 0
        case 6:
            return 0
        case 7:
            return  kitchens.count > 0 ? (headerHeight * 2) : 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return bannerImg.count > 0 ? 190 : 0
        case 1:
            return favCuisineShops.count > 0 ? 300 : 0
        case 2:
            return freeDeliverShops.count > 0 ? 300: 0
        case 3:
            return partyCateringShops.count > 0 ? 300: 0
        case 4:
            return mealServiceShops.count > 0 ? 300: 0
        case 5:
            return 190
        case 6:
            return 70
        case 7:
            return 140
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.CollectionTableCell, for: indexPath) as! CollectionTableCell
            cell.updateCell(bannerArray: self.bannerImg)
            cell.delegate = self
            cell.viewType = .banner
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.FavouriteCollectionTableViewCell, for: indexPath) as! FavouriteCollectionTableViewCell
            cell.updateCellForFavShops(collectionViewType: .favourite, shopsArray: self.favCuisineShops, isFilterApplied: !isAppliedFilter())
            cell.delegate = self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.FavouriteCollectionTableViewCell, for: indexPath) as! FavouriteCollectionTableViewCell
            cell.updateCellforFreeDelivery(collectionViewType: .free, shopsArray: self.freeDeliverShops, isFilterApplied: !isAppliedFilter())
            cell.delegate = self
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.FavouriteCollectionTableViewCell, for: indexPath) as! FavouriteCollectionTableViewCell
            cell.updateCellForCommon(collectionViewType: .partyCatering, shopsArray: self.partyCateringShops, isFilterApplied: !isAppliedFilter())
            cell.delegate = self
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.FavouriteCollectionTableViewCell, for: indexPath) as! FavouriteCollectionTableViewCell
            cell.updateCellForCommon(collectionViewType: .mealService, shopsArray: self.mealServiceShops, isFilterApplied: !isAppliedFilter())
            cell.delegate = self
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.CollectionTableCell, for: indexPath) as! CollectionTableCell
            cell.updateSuggestionCell(cusinesArray: self.suggestedCusines)
            cell.viewType = .suggestion
            cell.delegate = self
            return cell
        case 6:
            let sortCell = tableView.dequeueReusableCell(withIdentifier: HomeSortTableViewCell.cellIdentifier) as! HomeSortTableViewCell
            sortCell.delegate = self
            sortCell.updateCell(titleArray: sortArray, selectedIndex: selectedSortIndex)
            return sortCell
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.OrderListCell, for: indexPath) as! OrderListCell
            let shopListEntity = kitchens[indexPath.row]
            cell.set(values: shopListEntity)
            cell.imageoffers.rotate360Degrees()
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 7:
            let shopListEntity = kitchens[indexPath.row]
            if shopListEntity.shopstatus == SHOPSTATUS().open {
                let redirectTest = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.ResturantMenuListViewController) as! ResturantMenuListViewController
                redirectTest.shop = shopListEntity.id
                redirectTest.isHalal = shopListEntity.halal == 1 ? true : false
                redirectTest.shopList = shopListEntity
                self.navigationController?.pushViewController(redirectTest, animated: true)
            }else{
                self.showToast(string: APPLocalize.localizestring.shopClosed.localize())
            }
            break
        default:
            break
        }
    }
}

//MARK: - Location Manager & PostViewProtocol

extension HomeViewController: PostViewProtocol {
    
    func getSettingsData(api: Base, data: SettingsEntity?)
    {
        //  Common.storeUserData(from: data)
        print("Check >>",data?.ios_api_key ?? "")
        GMSServices.provideAPIKey(googleMapKey)
        GMSPlacesClient.provideAPIKey(googleMapKey)
        //googleMapKey = data?.ios_api_key ?? ""
        getUserProfileDetails()
        if appDelegate.isSkip {
            if let items =  self.tabBarController?.tabBar.items {
                
                for i in 0 ..< items.count {
                    let itemToDisable = items[i]
                    itemToDisable.isEnabled = true
                }
            }
            
        }
        getCousineList()
        
        
    }
    
    func getUserProfileDetails() {
        if let items =  self.tabBarController?.tabBar.items {
            
            for i in 0 ..< items.count {
                let itemToDisable = items[i]
                itemToDisable.isEnabled = false
            }
        }
        
        if User.main.accessToken != nil {
            self.presenter?.get(api: .userProfile, data: nil)
        } else {
            
            self.userLocation = UserCurrentLocation()
            self.userLocation?.latitude = currentLocation.latitude
            self.userLocation?.longitude = currentLocation.longitude
            if getFilterDetails().count > 0 {
                
                
                var filterParam = [String:AnyObject]()
                filterParam["latitude"] = currentLocation.latitude as AnyObject
                filterParam["longitude"] = currentLocation.longitude as AnyObject
                if(User.main.id != 0){
                    filterParam["user_id"] = User.main.id as AnyObject
                }
                let allFiltered = getFilterDetails().map({$0.value.filter({$0.state == true})}).flatMap({$0})
                let cuisinefiltered = allFiltered.filter({$0.type == APPLocalize.localizestring.Cuisines.localize()})
                let restaturntFilter = allFiltered.filter({$0.type == APPLocalize.localizestring.ShowRestaurantsWith.localize()})
                filterParam["filter"] = "normal" as AnyObject
                if cuisinefiltered.count > 0 {
                    //self.userLocation?.cuisine = cuisinefiltered.map({$0.id})
                    for (index,cuisineID) in cuisinefiltered.enumerated() {
                        //filterParam.append("cuisine[\(index)] = \(cuisineID)")
                        filterParam["cuisine[\(index)]"] = cuisineID.id as AnyObject
                    }
                    
                }
                if restaturntFilter.count > 0 {
                    let pureVeg = allFiltered.filter({$0.name == APPLocalize.localizestring.PureVeg.localize()})
                    let offer = allFiltered.filter({$0.name == APPLocalize.localizestring.Offer.localize()})
                    if pureVeg.count == 1 {
                        //self.userLocation?.pure_veg = 1
                        filterParam["pure_veg"] = 1 as AnyObject
                    }
                    if offer.count == 1 {
                        //self.userLocation?.offer = 1
                        filterParam["offer"] = 1 as AnyObject
                    }
                }
                if let theJSONData = try?  JSONSerialization.data(withJSONObject: filterParam,options: .prettyPrinted){
                    self.presenter?.get(api: .shopList, data: theJSONData)
                }
                
                
            } else {
                self.presenter?.get(api: .shopList, data: self.userLocation?.toData())
            }
            
        }
        
    }
    
    func getCousineList(){
        self.presenter?.get(api: .cuisines, data: nil)
    }
    
    
    
    func onError(api: Base, message: String, statusCode code: Int) {
        refreshControl.endRefreshing()
        shimmerView.isHidden = true
        if(api == .shopList){
            
        }else{
            if message.lowercased().contains("unauthenticated"){
                let alertController = UIAlertController(title: "", message: "Session expired", preferredStyle: .alert)

                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    let signInSkipVC = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.SignSkipViewController)
                    self.navigationController?.setViewControllers([signInSkipVC], animated: true)
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)

            }else{
                self.showToast(string: message)
            }
        }
        
        //        if(UIApplication.shared.isIgnoringInteractionEvents){
        //
        //            UIApplication.shared.endIgnoringInteractionEvents()
        //
        //        }
        
    }
    
    func getUserProfile(api: Base, data: UserProfileDetails?) {
        refreshControl.endRefreshing()
        
        self.loader.isHidden = true
        
        if let items =  self.tabBarController?.tabBar.items {
            
            for i in 0 ..< items.count {
                let itemToDisable = items[i]
                itemToDisable.isEnabled = true
            }
        }
        if api == .userProfile, data != nil {
            //            if(UIApplication.shared.isIgnoringInteractionEvents){
            //
            //                UIApplication.shared.endIgnoringInteractionEvents()
            //
            //            }
            Common.storeUserData(from: data)
            var cartCount = [Int]()
            if data?.cart?.count ?? 0 > 0 {
                
                for item in (data?.cart)! {
                    cartCount.append(Int(item.quantity ?? 0.0))
                }
            }
            
            if let address = data?.addresses {
                DataManager.shared.setUserAddressDetails(addressArray: address)
            }
            
            let totalcartCount = cartCount.reduce(0, +)
            
            if totalcartCount > 0 {
                User.main.cartCount = totalcartCount
            }
            
            User.main.referral_code = data?.referral_code
            self.userLocation = UserCurrentLocation()
            self.userLocation?.latitude = currentLocation.latitude
            self.userLocation?.longitude = currentLocation.longitude
            self.userLocation?.user_id = User.main.id
            
            
            //                var filterParam = [String:AnyObject]()
            //                filterParam["latitude"] = currentLocation.latitude as AnyObject
            //                filterParam["longitude"] = currentLocation.longitude as AnyObject
            //                if(User.main.id != 0){
            //                filterParam["user_id"] = User.main.id as AnyObject
            //            }
            self.restaurantFilter()
            
            
            
            //                print(filterParam)
            //                if let theJSONData = try?  JSONSerialization.data(withJSONObject: filterParam,options: .prettyPrinted){
            //                    self.presenter?.get(api: .shopList, data: theJSONData)
            //
            //                    self.loader.isHidden = false
            //
            //
            //
            //                }
            
            self.locationBtn.isUserInteractionEnabled = true
            
        }
    }
    
    func shopList(api: Base, data: Json4Swift_Base?) {
        
        
        self.loader.isHidden = true
        
        refreshControl.endRefreshing()
        if api == .shopList, data != nil {
            bannerImg = (data?.banners)!
            shopList = (data?.shops)!
            mealServiceShops = (data?.mealShops ?? [])
            partyCateringShops = (data?.cateringShops ?? [])
             restitle = shopList.count > 0 ? APPLocalize.localizestring.totalResturant.localize() : ""
            
            self.favCuisineShops = (data?.favorite_cuisine_shop)!
            self.freeDeliverShops = (data?.freedelivery_shops)!

                
            self.kitchens = ((data?.shops)!)
            if isAppliedFilter(){
                bannerImg = [Banners]()
                getCousineList()
                
            }
            
            
            if  User.main.cart != nil {
                if let tabItems = tabBarController?.tabBar.items {
                    // In this case we want to modify the badge number of the third tab:
                    let tabItem = tabItems[2]
                    if User.main.cartCount == 0 || User.main.cartCount == nil {
                        tabItem.badgeValue = nil
                    }else{
                        tabItem.badgeValue = "\(User.main.cartCount ?? 0)"
                    }
                }
            }
            
            
            if shopList.count > 0 || bannerImg.count > 0 || self.favCuisineShops.count > 0 ||  self.freeDeliverShops.count > 0{
                shimmerView.isHidden = true
                self.tableView.backgroundView = nil
            } else {
                shimmerView.isHidden = true
                self.tableView.setBackgroundImageAndTitle(imageName: EmptyImage.restaurantEmpty.rawValue, title: APPLocalize.localizestring.noRestaurant.localize(), description: APPLocalize.localizestring.noRestaurantContent.localize())

            }
            getCousineList()
            
            for item in shopList{
                if let obj = item.cuisines{
                    for cusineItem in obj{
                        if !suggestedCusines.contains(where: {$0.id == cusineItem.id}){
                            suggestedCusines.append(cusineItem)
                        }
                    }
                }
            }
            
            self.tableView.reloadData()
            
            if self.favCuisineShops.count > 0 && self.freeDeliverShops.count > 0 {
                
                if filterViewIsApplied{
                    filterViewIsApplied = !filterViewIsApplied
                    tableView.scrollToRow(at: IndexPath(row: 0, section: 2), at: .top, animated: true)
                }
            }
            self.favcollectionView.reloadData()
            self.FreeDeliverycollectionView.reloadData()
        }
        
    }
    
    func getCuisinesList(api: Base, data: [Cuisines]?) {
        
        //, msg: Message?
        
        if api == .cuisines, data != nil {
            constructFilterDataSource(data: data ?? [Cuisines]())
            self.cusinesList = data!
            //buttonFav.setImage(self.cusinesList.filter{$0.is_selected ?? false}.count > 0 ? UIImage(named: "fav-selected") : UIImage(named: "fav-unselected"), for: .normal)
            buttonFav.setSelected(selected: self.cusinesList.filter{$0.is_selected ?? false}.count > 0 ? true : false , animated: false)
            /*      if isCuisinesShown == f
             {
             self.foodCollection(cusines: self.cusinesList)
             
             }else{
             
             }*/
            
            var arr = [Bool]()
            
            for cuisine in self.cusinesList {
                
                arr.append(cuisine.is_selected!)
                
            }
            if arr.contains(true) {
                
                return
                
            }else
            {
                if !isCuisinesShown {
                    
                    self.foodCollection(cusines: self.cusinesList)
                    
                }
                
            }
            
        }
        
        
        
    }
    
    func constructFilterDataSource(data:[Cuisines]) {
        
        DispatchQueue.main.async {
            
            var restaurantsWithArray = [FilterItems(name: APPLocalize.localizestring.Offer.localize(), state: false, id: 0, type: APPLocalize.localizestring.ShowRestaurantsWith.localize()),FilterItems(name: APPLocalize.localizestring.PureVeg.localize(), state: false, id: 0, type: APPLocalize.localizestring.ShowRestaurantsWith.localize())]
            
            var tempArray = [FilterItems]()
            
            for cusine in data {
                let filterObject =  FilterItems(name: cusine.name ?? "", state: false, id: cusine.id ?? 0, type: APPLocalize.localizestring.Cuisines.localize())
                tempArray.append(filterObject)
            }
            restaurantsWithArray += tempArray
            
            let applied = getFilterDetails().map({$0.value.filter({$0.state == true})}).flatMap({$0})
            
            for appliedObject in applied {
                for (index,defaultObject) in restaurantsWithArray.enumerated() {
                    if defaultObject.id == appliedObject.id && defaultObject.name == appliedObject.name {
                        restaurantsWithArray.remove(at: index)
                        restaurantsWithArray.insert(appliedObject, at: index)
                    }
                }
            }
            
            self.filterDetailsArray = Dictionary(grouping: restaurantsWithArray, by: {$0.type})
            
            self.resturantFilterArray = []
            for item in self.filterDetailsArray{
                var filterObj = ResturantFilter()
                filterObj.title = item.key
                filterObj.items = item.value
                self.resturantFilterArray.append(filterObj)
            }
            self.resturantFilterArray = self.resturantFilterArray.sorted(by: {$0.title > $1.title})
            
        }
    }
}

//MARK: - Location Manager & CLLocationManagerDelegat
extension HomeViewController: CLLocationManagerDelegate {
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        currentLocation.latitude = (location?.coordinate.latitude)!
        currentLocation.longitude = (location?.coordinate.longitude)!
        
        let locations = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(locations, completionHandler: {(placemarks, error) in
            if (error != nil) {
                
            }
            
            var userAddress = String()
            
            if (Reachability()?.connection != Reachability.Connection.none) {
                if  let placemark = placemarks{
                    if placemark.count > 0 {
                        let placemark = placemarks![0]
                        
                        if placemark.name != nil {
                            
                            userAddress =  userAddress + placemark.name! + ", "
                        }
                        if placemark.thoroughfare != nil {
                            userAddress = userAddress + placemark.thoroughfare! + ", "
                        }
                        if placemark.subThoroughfare != nil {
                            
                            userAddress =  userAddress + placemark.subThoroughfare! + ", "
                        }
                        if placemark.locality != nil {
                            userAddress =  userAddress + placemark.locality! + ", "
                        }
                        if placemark.subLocality != nil {
                            // self.locationLbl.text = placemark.subLocality
                            userAddress =  userAddress + placemark.subLocality! + ", "
                            
                        }
                        if placemark.administrativeArea != nil {
                            
                            userAddress =  userAddress + placemark.administrativeArea! + ", "
                        }
                        if placemark.subAdministrativeArea != nil {
                            
                            userAddress =  userAddress + placemark.subAdministrativeArea! + ","
                        }
                        if placemark.postalCode != nil {
                            
                            userAddress =  userAddress + placemark.postalCode! + ","
                        }
                        if placemark.isoCountryCode != nil {
                            
                            userAddress =  userAddress + placemark.isoCountryCode! + ","
                        }
                        
                        if placemark.country != nil {
                            
                            userAddress =  userAddress + placemark.country!
                        }
                        
                        print("userAddress:",userAddress)
                        self.addressLbl.text = userAddress.localize()
                        self.getUserProfileDetails()
                    }
                }
            }
        })
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}



extension HomeViewController: SavedAddressDelegate {
    
    func didReceiveSavedAddress(isUpdated: Bool?, addressDetails: UserAddressDetails?) {
        
        if isUpdated == true {
            
            locationLbl.text = addressDetails?.type?.uppercased()
            addressLbl.text = addressDetails?.map_address
            currentLocation.latitude = (addressDetails?.latitude)!
            currentLocation.longitude = (addressDetails?.longitude)!
            
            self.userLocation = UserCurrentLocation()
            self.userLocation?.latitude = currentLocation.latitude
            self.userLocation?.longitude = currentLocation.longitude
            self.userLocation?.user_id = User.main.id
            self.shimmerView.isHidden = false
            self.presenter?.get(api: .shopList, data: self.userLocation?.toData())
            
        }
    }
    
}


extension HomeViewController{
    
    
    func showBgView(hidden:Bool?){
        if hidden == true{
            
            if bgImg != nil{
                
                self.bgImg?.removeFromSuperview()
            }
            
        }else{
            
            if bgImg == nil{
                
                bgImg = UIImageView()
                bgImg?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                bgImg?.backgroundColor = .black
                bgImg?.alpha = 0.4
                self.view.addSubview(bgImg!)
            }
        }
    }
}



extension Array {
    
    init?(jsonString: String) {
        guard let array = (
            try? JSONSerialization.jsonObject(with: Data(jsonString.utf8))
            ) as? [Element] else { return nil }
        
        self.init(array)
    }
}

extension HomeViewController {
    func restaurantFilter(){
        if getFilterDetails().count > 0 {
            var filterParam = [String:AnyObject]()
            filterParam["latitude"] =  self.currentLocation.latitude as AnyObject
            filterParam["longitude"] = self.currentLocation.longitude as AnyObject
            filterParam["filter"] = "normal" as AnyObject
            if(User.main.id != 0){
                filterParam["user_id"] = User.main.id as AnyObject
            }
            let allFiltered = getFilterDetails().map({$0.value.filter({$0.state == true})}).flatMap({$0})
            let cuisinefiltered = allFiltered.filter({$0.type == APPLocalize.localizestring.Cuisines.localize()})
            let restaturntFilter = allFiltered.filter({$0.type == APPLocalize.localizestring.ShowRestaurantsWith.localize()})
            if cuisinefiltered.count > 0 {
                //self.userLocation?.cuisine = cuisinefiltered.map({$0.id})
                for (index,cuisineID) in cuisinefiltered.enumerated() {
                    //filterParam.append("cuisine[\(index)] = \(cuisineID)")
                    filterParam["cuisine[\(index)]"] = cuisineID.id as AnyObject
                }
                
            }
            if restaturntFilter.count > 0 {
                let pureVeg = allFiltered.filter({$0.name == APPLocalize.localizestring.PureVeg.localize()})
                let offer = allFiltered.filter({$0.name == APPLocalize.localizestring.Offer.localize()})
                if pureVeg.count == 1 {
                    //self.userLocation?.pure_veg = 1
                    filterParam["pure_veg"] = 1 as AnyObject
                }
                if offer.count == 1 {
                    //self.userLocation?.offer = 1
                    filterParam["offer"] = 1 as AnyObject
                }
            }
            if let theJSONData = try?  JSONSerialization.data(withJSONObject: filterParam,options: .prettyPrinted){
                self.presenter?.get(api: .shopList, data: theJSONData)
            }
        }else {
            var filterParam = [String:AnyObject]()
            filterParam["latitude"] =  self.currentLocation.latitude as AnyObject
            filterParam["longitude"] = self.currentLocation.longitude as AnyObject
            if(User.main.id != 0){
                filterParam["user_id"] = User.main.id as AnyObject
            }
            if let theJSONData = try?  JSONSerialization.data(withJSONObject: filterParam,options: .prettyPrinted){
                self.presenter?.get(api: .shopList, data: theJSONData)
            }
        }
        
    }
    
}

extension HomeViewController: HomeBannerDelegate{
    
    
    func didSelectBanner(bannerObj: Banners) {
        
        if bannerObj.shopstatus == SHOPSTATUS().open {
            let redirectTest = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.ResturantMenuListViewController) as! ResturantMenuListViewController
            redirectTest.shop = bannerObj.shop?.id
            redirectTest.shopList = bannerObj.shop
            redirectTest.isHalal = bannerObj.shop?.halal == 1 ? true : false
            redirectTest.shopList = bannerObj.shop
            self.navigationController?.pushViewController(redirectTest, animated: true)
        }else {
            self.showToast(string: APPLocalize.localizestring.shopClosed.localize())
        }
    }
    
    func didSelectCusines(index: Int) {
        
        let cusinesObj = self.suggestedCusines[index]
        
        let suggestionVC = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.CusinesSuggestionViewController) as! CusinesSuggestionViewController
        
        var kitchenList: [Shops] = []
        for item in shopList{
            if (item.cuisines?.contains(where: {$0.id == cusinesObj.id}))!{
                kitchenList.append(item)
            }
        }
        
        suggestionVC.kitchenArray = kitchenList
        self.navigationController?.pushViewController(suggestionVC, animated: true)
    }
}

extension HomeViewController: FavouriteTableViewCellDelegate{
    
    func getSelectedShop(collectionType: HomeCollectionViewType, index: Int) {
        
        if collectionType == .favourite{
            let shopListEntity = self.favCuisineShops[index]
            if shopListEntity.shopstatus == SHOPSTATUS().open {
                let redirectTest = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.ResturantMenuListViewController) as! ResturantMenuListViewController
                redirectTest.isHalal = shopListEntity.halal == 1 ? true : false
                
                redirectTest.shop = self.favCuisineShops[index].id//shopListEntity.shop?.id
                redirectTest.shopList = self.favCuisineShops[index]
                redirectTest.shopList = shopListEntity
                
                self.navigationController?.pushViewController(redirectTest, animated: true)
            } else {
                
                self.showToast(string: APPLocalize.localizestring.shopClosed.localize())
            }
        }else if collectionType == .free{
            
            let shopListEntity = self.freeDeliverShops[index]
            if shopListEntity.shopstatus == SHOPSTATUS().open {
                let redirectTest = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.ResturantMenuListViewController) as! ResturantMenuListViewController
                
                redirectTest.shop = self.freeDeliverShops[index].id//shopListEntity.shop?.id
                redirectTest.shopList = self.freeDeliverShops[index]
                redirectTest.isHalal = shopListEntity.halal == 1 ? true : false
                redirectTest.shopList = shopListEntity
                redirectTest.isFreeDelivery = true
                self.navigationController?.pushViewController(redirectTest, animated: true)
            } else {
                
                self.showToast(string: APPLocalize.localizestring.shopClosed.localize())
            }
        }else if collectionType == .partyCatering{
            
            let shopListEntity = self.partyCateringShops[index]
            if shopListEntity.shopstatus == SHOPSTATUS().open {
                let redirectTest = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.ResturantMenuListViewController) as! ResturantMenuListViewController
                
                redirectTest.shop = self.partyCateringShops[index].id
                redirectTest.shopList = self.partyCateringShops[index]
                redirectTest.isHalal = shopListEntity.halal == 1 ? true : false
                redirectTest.shopList = shopListEntity
                self.navigationController?.pushViewController(redirectTest, animated: true)
            } else {
                
                self.showToast(string: APPLocalize.localizestring.shopClosed.localize())
            }
        }else if collectionType == .mealService{
            let shopListEntity = self.mealServiceShops[index]
            if shopListEntity.shopstatus == SHOPSTATUS().open {
                let redirectTest = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.ResturantMenuListViewController) as! ResturantMenuListViewController
                
                redirectTest.shop = self.mealServiceShops[index].id
                redirectTest.shopList = self.mealServiceShops[index]
                redirectTest.isHalal = shopListEntity.halal == 1 ? true : false
                redirectTest.shopList = shopListEntity
                self.navigationController?.pushViewController(redirectTest, animated: true)
            } else {
                
                self.showToast(string: APPLocalize.localizestring.shopClosed.localize())
            }
        }
    }
}

extension HomeViewController: HomeSortTableViewCellDelegate{
    func getSelected(index: Int) {
        selectedSortIndex = index
        switch index {
        case SortType.deliveryType.rawValue:
            showDeliveryTypeView()
            break
        case SortType.rating.rawValue:
            showRatingView()
            break
        case SortType.pickup.rawValue:
            let cell = tableView.cellForRow(at: IndexPath(item: 0, section: 6)) as! HomeSortTableViewCell
            self.sortArray = ["Delivery Type", "Rating", "Pickup"]
            cell.updateCell(titleArray: self.sortArray, selectedIndex: 2)
            kitchens = shopList.filter({ (shopObj) -> Bool in
                return (shopObj.deliveryoption!.filter{$0.name == "Takeaway"}).count > 0
            })
            tableView.reloadSections(IndexSet(integer: 7), with: .none)
            break
        case SortType.reset.rawValue:
            kitchens = shopList
            let cell = tableView.cellForRow(at: IndexPath(item: 0, section: 6)) as! HomeSortTableViewCell
            self.sortArray = ["Delivery Type", "Rating", "Pickup"]
            cell.updateCell(titleArray: self.sortArray, selectedIndex: nil)
            self.selectedSortIndex = nil
            tableView.reloadSections(IndexSet(integer: 7), with: .none)
            break
        default:
            break
        }
    }
    
    
    func showRatingView() {
        
        let alertController = RatingRangeViewController()
        _ = alertController.view
        alertController.delegate = self
        let alertPresentation = BottomAlertPresentation(from: self, to: alertController)
               alertController.transitioningDelegate = alertPresentation
               present(alertController, animated: true)
    }
    
    func showDeliveryTypeView() {
        
        let alertController = SortDeliveryTypeViewController()
        _ = alertController.view
        alertController.delegate = self
        let alertPresentation = BottomAlertPresentation(from: self, to: alertController)
               alertController.transitioningDelegate = alertPresentation
               present(alertController, animated: true)
    }
}

extension HomeViewController: RatingRangeViewDelegate{
    
    func getSliderValue(value: Int) {
        
        let cell = tableView.cellForRow(at: IndexPath(item: 0, section: 6)) as! HomeSortTableViewCell
        self.sortArray =  ["Delivery Type", "\(value) ★", "Pickup"]
        cell.updateCell(titleArray: self.sortArray, selectedIndex: 1)
        kitchens = shopList.filter{($0.rating ?? 0) >= value}
        tableView.reloadSections(IndexSet(integer: 7), with: .none)
    }
}

extension HomeViewController: SortDeliveryDelegate{
    
    func getSelectedType(type: Int) {
        if type == 0{
            let cell = tableView.cellForRow(at: IndexPath(item: 0, section: 6)) as! HomeSortTableViewCell
            self.sortArray = ["Delivery fee", "Rating", "Pickup"]
            cell.updateCell(titleArray: self.sortArray, selectedIndex: 0)
            kitchens = shopList.filter({ (shopObj) -> Bool in
                return (shopObj.deliveryoption!.filter{$0.name != "Takeaway"}).count > 0
            })
            tableView.reloadSections(IndexSet(integer: 7), with: .none)
        }else{
            
            let cell = tableView.cellForRow(at: IndexPath(item: 0, section: 6)) as! HomeSortTableViewCell
            self.sortArray = ["Free delivery", "Rating", "Pickup"]
            cell.updateCell(titleArray: self.sortArray, selectedIndex: 0)
            kitchens = shopList.filter{$0.free_delivery == 1}
            tableView.reloadSections(IndexSet(integer: 7), with: .none)
        }
    }
}
