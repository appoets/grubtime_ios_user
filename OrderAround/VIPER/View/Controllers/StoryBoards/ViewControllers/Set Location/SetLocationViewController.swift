//
//  SetLocationViewController.swift
//  orderAround
//
//  Created by Basha's MacBook Pro on 23/09/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit

class SetLocationViewController: UIViewController {
    
    @IBOutlet weak var useCurrentLocaton: UIButton!
    @IBOutlet weak var setManualLocation: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        self.design()
    }
    
    func localize() {
        
//        self.mobileNumberTxtFlb.placeholder = APPLocalize.localizestring.MobileNumber.localize()
//        self.passwordTxtFld.placeholder =  APPLocalize.localizestring.password.localize()
//        self.connectWithSocialLbl.text =  APPLocalize.localizestring.copnnectWithSocial.localize()
//
//        self.accountBut.setTitle( APPLocalize.localizestring.dontHaveAccount.localize(), for: .normal)
//        self.forgetPasswordBut.setTitle(APPLocalize.localizestring.forgetPassword.localize(), for: .normal)
//        self.signInBut.setTitle(APPLocalize.localizestring.signIn.localize(), for: .normal)
        
    }
    
    func design() {
        useCurrentLocaton.layer.cornerRadius = 16
       // useCurrentLocaton.layer.borderWidth = 1
        
        Common.setFont(to: useCurrentLocaton, isTitle: true, size: 18, fontType: .semiBold)
        Common.setFont(to: setManualLocation, isTitle: true, size: 18, fontType: .semiBold)
    }
    
    
    @IBAction func uselocation(sender:Any){
        
        push(id: Storyboard.Ids.SaveDeliveryLocationViewController, animation: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
