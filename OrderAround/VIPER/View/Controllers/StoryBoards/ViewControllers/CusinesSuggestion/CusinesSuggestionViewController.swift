//
//  CusinesSuggestionViewController.swift
//  orderAround
//
//  Created by Prem's on 27/11/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class CusinesSuggestionViewController: UIViewController {

    @IBOutlet var kitchenTableView: UITableView!
    
    var kitchenArray: [Shops] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        
        kitchenTableView.register(OrderListCell.nib, forCellReuseIdentifier: OrderListCell.cellIdentifier)
        kitchenTableView.tableFooterView = UIView()
    }
    
    //MARK:- Show Custom Toast
    private func showToast(string : String?) {
        self.view.makeToast(string, point: CGPoint(x: UIScreen.main.bounds.width/2 , y: UIScreen.main.bounds.height/2), title: nil, image: nil, completion: nil)
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension CusinesSuggestionViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return kitchenArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 160
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderListCell.cellIdentifier, for: indexPath) as! OrderListCell
        cell.set(values: kitchenArray[indexPath.row])
        cell.imageoffers.rotate360Degrees()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let shopListEntity = kitchenArray[indexPath.row]
        if shopListEntity.shopstatus == SHOPSTATUS().open {
            let redirectTest = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.ResturantMenuListViewController) as! ResturantMenuListViewController
            redirectTest.shop = shopListEntity.id
            redirectTest.isHalal = shopListEntity.halal == 1 ? true : false
            redirectTest.shopList = shopListEntity
            self.navigationController?.pushViewController(redirectTest, animated: true)
        }else{
            self.showToast(string: APPLocalize.localizestring.shopClosed.localize())
        }
    }
}
