//
//  ChooseLoginTypeViewController.swift
//  orderAround
//
//  Created by Basha's MacBook Pro on 21/09/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit


class ChooseLoginTypeViewController: UIViewController {

    //MARK: Declaration.

    
    @IBOutlet weak var fb_bg_Img: UIImageView!
    @IBOutlet weak var g_bg_Img: UIImageView!
    @IBOutlet weak var email_bg_Img: UIImageView!

    @IBOutlet weak var bottom_bg_Img: UIImageView!

    
    @IBOutlet weak var fb_login_btn: UIButton!
    @IBOutlet weak var google_btn: UIButton!
    @IBOutlet weak var email_btn: UIButton!
    @IBOutlet weak var login_LaterBtn: UIButton!
    
    
    @IBOutlet weak var fbView: UIView!
    @IBOutlet weak var googleView: UIView!
    @IBOutlet weak var emailView: UIView!

    @IBOutlet var termsLbl: UILabel!
    
    private lazy var  loader = {
        return createActivityIndicator(UIApplication.shared.keyWindow ?? self.view)
    }()
    fileprivate var socialLogin = SocialLoginHelper ()
    
    let facebook = "fb"
    let google = "google"
     private var apiTypeStr = ""
      private var userData = LoginRequestData()
     private var accessToken : String?
    //MARK: View Life Cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        localize()
        design()
        
        self.bottom_bg_Img.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        enableKeyboardHandling()
        
        self.navigationController?.isNavigationBarHidden = true
        setupTermsLbl()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        disableKeyboardHandling()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        DispatchQueue.main.async {
        
            self.bottom_bg_Img.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
            self.fb_bg_Img.roundCorners(corners: [.topLeft ,.bottomLeft], radius: 14.0)
            self.g_bg_Img.roundCorners(corners: [.topLeft ,.bottomLeft], radius: 14.0)
            self.email_bg_Img.roundCorners(corners: [.topLeft ,.bottomLeft], radius: 14.0)


        }

    }
    
    func setupTermsLbl() {
        
        
        let text = "By continuing, You agree to the \n Terms of Services and Privacy Policy"
        termsLbl.text = text
        termsLbl.font = UIFont.Regular()
        self.termsLbl.textColor =  UIColor.lightGray
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms of Services")
        let range2 = (text as NSString).range(of: "Privacy Policy")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value: UIFont.BoldRegular(), range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.font, value:UIFont.BoldRegular(), range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.primary, range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.primary, range: range2)
        termsLbl.attributedText = underlineAttriString
        termsLbl.isUserInteractionEnabled = true
        //termsLbl.lineBreakMode = .byWordWrapping
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture.numberOfTouchesRequired = 1
        termsLbl.addGestureRecognizer(tapGesture)
    }
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = termsLbl.text else { return }
        let numberRange = (text as NSString).range(of: "Terms of Services")
        let emailRange = (text as NSString).range(of: "Privacy Policy")
        let termsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Storyboard.Ids.TermsViewController) as! TermsViewController
        if gesture.didTapAttributedTextInLabel(label: self.termsLbl, inRange: numberRange) {
            print("terms tapped")
            termsVC.viewType = .terms
        } else if gesture.didTapAttributedTextInLabel(label: self.termsLbl, inRange: emailRange) {
            print("Privacy Policy tapped")
            termsVC.viewType = .privacyPolicy
        }
        self.navigationController?.pushViewController(termsVC, animated: true)
    }
    
    @IBAction func gmailButtonfunc(_ sender: Any) {
        
        socialLogin.loginThroughGoogle(fromViewController: self, helperDelegate: self )
        
    }
    
    
    @IBAction func facebookBtn(sender:Any){
        
        socialLogin.loginThroughFacebook(fromViewController: self, helperDelegate: self)
        

    }
    
 
 
    @IBAction func emailBtn(sender:Any){
        
        self.push(id: Storyboard.Ids.SignInViewController, animation: true)

    }
    
    @IBAction func tapLoginLater() {
        appDelegate.isSkip = true
        let baseHomeVC = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.BaseTabController)
        self.navigationController?.pushViewController(baseHomeVC, animated: true)
    }
    
    private func showToast(string : String?) {
        
        self.view.makeToast(string, point: CGPoint(x: UIScreen.main.bounds.width/2 , y: UIScreen.main.bounds.height/2), title: nil, image: nil, completion: nil)
        
    }
    
    func localize() {
        
        fb_login_btn.layer.cornerRadius = 16
      //  fb_login_btn.layer.borderWidth = 1
        google_btn.layer.cornerRadius = 16
      //  google_btn.layer.borderWidth = 1
        email_btn.layer.cornerRadius = 16
      //  email_btn.layer.borderWidth = 1
        self.fb_login_btn.setTitle(APPLocalize.localizestring.fblogin.localize(), for: .normal)
        self.google_btn.setTitle(APPLocalize.localizestring.googleLogin.localize(), for: .normal)
        self.email_btn.setTitle(APPLocalize.localizestring.emailLogin.localize(), for: .normal)
        self.login_LaterBtn.setTitle(APPLocalize.localizestring.loginLater.localize(), for: .normal)
    }
    
    func design() {
        
        Common.setFont(to: fb_login_btn, isTitle: true, size: 18, fontType: .medium)
        Common.setFont(to: google_btn, isTitle: true, size: 18, fontType: .medium)
        Common.setFont(to: email_btn, isTitle: true, size: 18, fontType: .medium)
        Common.setFont(to: login_LaterBtn, isTitle: true, size: 18, fontType: .medium)

    }

}
    
extension ChooseLoginTypeViewController: PostViewProtocol {

    func onError(api: Base, message: String, statusCode code: Int) {
        print("onerror>>>>>",code)
        print("messgae >>>",message)
        
        
        if code == StatusCode.notRegistered.rawValue {
        let mobileVc = self.storyboard?.instantiateViewController(withIdentifier: Storyboard.Ids.MobileViewController) as? MobileViewController
         mobileVc?.userData = self.userData
        mobileVc?.apiType = self.apiTypeStr
        mobileVc?.isFromSocialLogin = true
        self.navigationController?.pushViewController(mobileVc!, animated: true)
            
        }
    }
    
    func getOath(api: Base, data: LoginRequestData?) {
        print("acess>>",data?.accessToken)
        print("ACCss>>>>",data?.access_token)
        
        if api == .login, let accessToken = data?.access_token {
            User.main.accessToken = accessToken
            User.main.refreshToken = data?.refresh_token
            
            self.loader.isHidden = true
            storeInUserDefaults()
            self.showToast(string: APPLocalize.localizestring.loginSuccess.localize())
            appDelegate.isSkip = false
            let baseHomeVC = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.BaseTabController)
            self.navigationController?.pushViewController(baseHomeVC, animated: true)
            
        } else if api == .socialLogin , let accessToken = data?.access_token {
            print("Access>>>>",data?.access_token)
            print("Acc>>>",data?.accessToken)
            
            User.main.accessToken = accessToken
            User.main.refreshToken = data?.refresh_token
            
            self.loader.isHidden = true
            storeInUserDefaults()
            appDelegate.isSkip = false
            self.showToast(string: APPLocalize.localizestring.loginSuccess.localize())
            let baseHomeVC = Router.main.instantiateViewController(withIdentifier: Storyboard.Ids.BaseTabController)
            self.navigationController?.pushViewController(baseHomeVC, animated: true)
            
        }

    
}
}


extension ChooseLoginTypeViewController: SocialLoginHelperDelegate {
    func didReceiveFacebookLoginUser(detail: FacebookUserDetail) {
        
        print("*******FDSK",detail.email)
        var loginRequestData = LoginRequestData()
        loginRequestData.login_by = .facebook
        if let token = UserDefaults.standard.value(forKey: Keys.list.socialLoginAccessToken) as? String {
            loginRequestData.accessToken = token

        }
        loginRequestData.name = "\(detail.firstName) \(detail.firstName)"
        loginRequestData.email = detail.email
        self.userData = loginRequestData
        apiTypeStr = "fb"
        self.presenter?.post(api: .socialLogin, data: loginRequestData.toData())
    }
    
    func didReceiveGoogleLoginUser(detail: GIDGoogleUser) {
         print("*******gmsil",detail.profile.email)
        
        
      // self.presenter.get(api: .login, url: )
        var loginRequestData = LoginRequestData()
        loginRequestData.login_by = .google
        loginRequestData.accessToken = detail.authentication.accessToken
        loginRequestData.email = detail.profile.email
        loginRequestData.name = detail.profile.name
        apiTypeStr = "google"
        self.userData = loginRequestData
        self.presenter?.post(api: .socialLogin, data: loginRequestData.toData())
        
    }
    
    func didReceiveFacebookLoginError(message: String) {
        
    }
    
    func didReceiveGoogleLoginError(message: String) {
        
    }
    
    
}


