//
//  RefundHistoryViewController.swift
//  orderAround
//
//  Created by Prem's on 29/09/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class RefundHistoryViewController: UIViewController {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var historyTableView: UITableView!
    
    var historyArray: [RefundHistoryData] = []
    private lazy var loader  : UIView = {
        return createActivityIndicator(self.view)
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialLoads()
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    private func initialLoads() {
        
        historyTableView.register(UINib(nibName: XIB.Names.RefundHistoryTableViewCell, bundle: nil), forCellReuseIdentifier: XIB.Names.RefundHistoryTableViewCell)
        historyTableView.tableFooterView = UIView()
        localize()
        setFont()
        getRefundHistoryAPI()
    }
    
    private func localize() {
        titleLbl.text = APPLocalize.localizestring.refundHistory.localize()
    }
    
    private func setFont() {
        Common.setFont(to: titleLbl)
    }
    
    private func getRefundHistoryAPI() {
        
        self.loader.isHidden = false
        self.presenter?.get(api: .refundHistory, data: nil)
    }

}

extension RefundHistoryViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.historyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let refundCell = tableView.dequeueReusableCell(withIdentifier: XIB.Names.RefundHistoryTableViewCell) as! RefundHistoryTableViewCell
        refundCell.updateCell(refundObj: historyArray[indexPath.row])
        return refundCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension RefundHistoryViewController: PostViewProtocol {
    
    func onError(api: Base, message: String, statusCode code: Int) {
        
        self.loader.isHidden = true
    }
    
    func getRefundHistoryData(api: Base, data: RefundHistoryModel?) {
           
        self.loader.isHidden = true
        self.historyArray = data?.refundHistories ?? []
        self.historyTableView.reloadData()
    }
}
