//
//  TermsViewController.swift
//  orderAround
//
//  Created by Prem's on 18/09/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit
import WebKit

enum TermsWebViewType{
    case terms
    case privacyPolicy
    case help
}

class TermsViewController: UIViewController {
    
    @IBOutlet var termWebView: WKWebView!
    @IBOutlet var titleLbl: UILabel!
    
    var viewType: TermsWebViewType = .terms
    
    private lazy var  loader = {
        return createActivityIndicator(self.view.window ?? self.view)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        termWebView.uiDelegate = self
        termWebView.navigationDelegate = self
//       iguration.mediaTypesRequiringUserActionForPlayback = .all
        initalLoads()
        self.loader.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.loader.isHidden = true
    }
    
    private func initalLoads(){
        
        switch viewType{
        case .terms:
            titleLbl.text = APPLocalize.localizestring.termsOfServiceTitle.localize()
            break
        case .privacyPolicy:
            titleLbl.text = APPLocalize.localizestring.privacyPolicyTitle.localize()
            break
        case .help:
            titleLbl.text = APPLocalize.localizestring.help.localize()
        }
        callwebView()
        
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
    
    private func callwebView(){
        var myURLString = ""
        
        switch viewType{
        case .terms:
            myURLString = "https://www.Grubtime.com.au/terms-of-use"
            break
        case .privacyPolicy:
            myURLString = "https://www.Grubtime.com.au/privacy-policy"
            break
        case .help:
            myURLString = "https://www.Grubtime.com.au/customer-help-page" //"https://www.Grubtime.com.au/help"
        }
        
        if let url = URL(string: myURLString){
            let request = URLRequest(url: url)
            termWebView.load(request)
        }
        
    }
    
    @objc func onbackAction(){
        self.navigationController?.popViewController(animated: true)
    }
}

extension TermsViewController: WKNavigationDelegate, WKUIDelegate{
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        
        self.loader.isHidden = true
        print(error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        print("Strat to load")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
         print("finish to load")
        self.loader.isHidden = true
    }
}
