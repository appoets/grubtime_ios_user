//
//  PresentationController.swift
//  orderAround
//
//  Created by Prem's on 12/11/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class PresentationController: UIPresentationController {
    private var calculatedFrameOfPresentedViewInContainer = CGRect.zero //swiftlint:disable:this identifier_name
    private var shouldSetFrameWhenAccessingPresentedView = false

    override var presentedView: UIView? {
        if shouldSetFrameWhenAccessingPresentedView {
            super.presentedView?.frame = calculatedFrameOfPresentedViewInContainer
        }

        return super.presentedView
    }

    override func presentationTransitionDidEnd(_ completed: Bool) {
        super.presentationTransitionDidEnd(completed)
        shouldSetFrameWhenAccessingPresentedView = completed
    }

    override func dismissalTransitionWillBegin() {
        super.dismissalTransitionWillBegin()
        shouldSetFrameWhenAccessingPresentedView = false
    }

    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()
        calculatedFrameOfPresentedViewInContainer = frameOfPresentedViewInContainerView
    }
}

