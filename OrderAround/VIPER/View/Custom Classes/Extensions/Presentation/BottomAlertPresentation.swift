//
//  BottomAlertPresentation.swift
//  orderAround
//
//  Created by Prem's on 12/11/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

final class BottomAlertPresentation: NSObject, UIViewControllerTransitioningDelegate {

    var dimBackground = true
    init(from presented: UIViewController, to presenting: UIViewController) {
        super.init()
        presenting.modalPresentationStyle = .custom
        presenting.modalTransitionStyle = .crossDissolve
    }

    func presentationController(forPresented presented: UIViewController,
                                presenting: UIViewController?,
                                source: UIViewController) -> UIPresentationController? {

        let controller = BottomAlertPresentationController(presentedViewController: presented,
                                                           presenting: presenting)
        controller.dimBackground = dimBackground
        return controller
    }
}

private final class BottomAlertPresentationController: PresentationController {
    var dimBackground = true
    private lazy var dimmingView: UIView! = {
        guard let container = containerView else { return nil }

        let view = UIView(frame: container.bounds)
        if dimBackground == true {
            let blurEffect = UIBlurEffect(style: .regular)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.addSubview(blurEffectView)
            view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        } else {
            view.backgroundColor = .clear
        }
        return view
    }()
    
    override var frameOfPresentedViewInContainerView: CGRect {
        guard let containerView = containerView,
            let presentedView = presentedView else { return .zero }

        let targetWidth = containerView.bounds.width
        let fittingSize = CGSize(
            width: targetWidth,
            height: 200
        )
        let targetHeight = presentedView.systemLayoutSizeFitting(fittingSize,
                                                                 withHorizontalFittingPriority: .required,
                                                                 verticalFittingPriority: .defaultLow).height

        let presentationHeight = min(targetHeight, (containerView.frame.height - 150))
        let presentedControllerFrame = CGRect(x: (containerView.frame.width - targetWidth) / 2,
                                              y: (containerView.bounds.height - presentationHeight),
                                              width: targetWidth,
                                              height: presentationHeight)
        return presentedControllerFrame
    }
    
    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()
        presentedView?.frame = frameOfPresentedViewInContainerView
    }

    override func presentationTransitionWillBegin() {
        guard let container = containerView,
            let coordinator = presentingViewController.transitionCoordinator else { return }

        dimmingView.alpha = 0
        container.addSubview(dimmingView)
        presentedViewController.view.layer.cornerRadius = 10.0

        coordinator.animate(alongsideTransition: { [weak self] _ in
            guard let `self` = self else { return }
            self.dimmingView.alpha = 1
        }, completion: nil)
    }
    
    override func dismissalTransitionWillBegin() {
        guard let coordinator = presentingViewController.transitionCoordinator else { return }

        coordinator.animate(alongsideTransition: { [weak self] _ in
            guard let `self` = self else { return }

            self.dimmingView.alpha = 0
        }, completion: nil)
    }

    override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            dimmingView.removeFromSuperview()
        }
    }
}
