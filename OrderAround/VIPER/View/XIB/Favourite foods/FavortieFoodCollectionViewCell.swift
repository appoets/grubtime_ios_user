//
//  FavortieFoodCollectionViewCell.swift
//  orderAround
//
//  Created by Basha's MacBook Pro on 23/09/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit

class FavortieFoodCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var favoriteLbl :UILabel?
    @IBOutlet var favoriteView :UIView?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        Common.setFont(to: favoriteLbl!, isTitle: false, size: 12, fontType: .regular)
       
    }

}
