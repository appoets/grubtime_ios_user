//
//  FavouriteFood.swift
//  orderAround
//
//  Created by Basha's MacBook Pro on 23/09/19.
//  Copyright © 2019 css. All rights reserved.
//

import Foundation
import UIKit


class FavouriteFood: UIView {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var listCollectionView: UICollectionView!
    
  //  var onDoneBtn : (( FavoriteModal)->Void)?
    var onDoneBtn : (( FavoriteModal)->Void)?
    var onClickButton : ((_ food : [Int])->Void)?
    var selectedFavourite = 0
    var selectedCell = [String]()
    var selectedID = [Int]()
    
    
    var data = [Cuisines]()
    override func awakeFromNib() {
        
          listCollectionView.allowsMultipleSelection = true
        super.awakeFromNib()
        listCollectionView.dataSource = self
         listCollectionView.delegate = self

         let nibPost = UINib(nibName: XIB.Names.FavortieFoodCollectionViewCell, bundle: nil)
            listCollectionView.register(nibPost, forCellWithReuseIdentifier: XIB.Names.FavortieFoodCollectionViewCell)
        doneBtn.addTarget(self, action: #selector(doneButtonAction), for: .touchUpInside)
        self.listCollectionView.reloadData()
       // setUpDummyData()
        
    }
    
    
   
 
    func setupSelections(list:[Cuisines]) {
        for cuisine in list {
            if cuisine.is_selected! {
                selectedCell.append(cuisine.name!)
                selectedID.append(cuisine.id!)
            }
            
        }
        
        if selectedID.count < self.data.count {
   
            let diffference = self.data.count - selectedID.count
            
//            for index in 0..<diffference {
//
//                selectedID.insert(-1, at: selectedID.count - 1)
//
//            }
   
            
        }
        
    }
 
      @IBAction func doneButtonAction() {
        
//        for index in 0..<self.selectedID.count{
//
//            if selectedID[index - 1] == -1 {
//
//                selectedID.remove(at: index - 1)
//            }
//
//        }
        
        
       
        
        
        for item in self.data {
            
           // self.selectedID.append(item.is_selected! ? item.id! : 0)
            
            if item.is_selected!
            {
                 self.selectedID.append(item.id!)
            }
            
            
            
            
        }
    
        
        self.onClickButton!(self.selectedID)
        
    }
    
    
//    override var isSelected: Bool {
//        didSet {
//            self.layer.borderWidth = 3.0
//            self.layer.borderColor = isSelected ? UIColor.blue.cgColor : UIColor.clear.cgColor
//        }
//    }
    
    
}



extension FavouriteFood : UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
      //  let yourHeight = yourWidth
        
        //return CGSize(width: yourWidth, height: 40)
        
        
        let nbCol = 3
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(nbCol))
        return CGSize(width: size + 5 , height: size/2)
        
//        let label = UILabel(frame: CGRect.zero)
//        label.text = data[indexPath.item].name
//        label.sizeToFit()
//        return CGSize(width: label.frame.width + 10, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
      //  return bannerImg.count > 0 ? bannerImg.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = listCollectionView.dequeueReusableCell(withReuseIdentifier: XIB.Names.FavortieFoodCollectionViewCell, for: indexPath) as! FavortieFoodCollectionViewCell
        cell.favoriteLbl?.text = data[indexPath.row].name?.uppercased()
        cell.layer.cornerRadius = 5
        
        if data[indexPath.row].is_selected!
       // if selectedID.contains(data[indexPath.row].id!)
        {
            cell.contentView.backgroundColor = #colorLiteral(red: 0.9375666976, green: 0.3109807074, blue: 0.3499014974, alpha: 1)
            cell.favoriteLbl?.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else {
            cell.contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell.favoriteLbl?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        }
        cell.favoriteLbl?.sizeToFit()
       // cell.contentView.backgroundColor = data[indexPath.row].is_selected! ? #colorLiteral(red: 0.8408991694, green: 0.1620869339, blue: 0.165025562, alpha: 1) : .white

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       print("didselected in Favourite>>>>>")
        
        selectedFavourite = indexPath.row
        print("selectedFavourite>>>",selectedFavourite)
        
//        for i in 0..<selectedCell.count{
//            print("i>>>>",i)
//        }
        
        
        
        if  data[indexPath.row].is_selected! {
            print("selectedCell>>>",selectedCell)
            print("indxepath>>>>",indexPath.row)
          // selectedCell.remove(at: indexPath.row)
           
       
              //  selectedCell.remove(at: indexPath.row)
                //selectedID.remove(at: indexPath.row)
                //selectedID.insert(-1, at: indexPath.row)
                          
                data[indexPath.row].is_selected = false
        
            
            
                listCollectionView.reloadData()

            
            
            
            
        }else  {
           // selectedCell.add(data[indexPath.row].name)
            
            //selectedCell.append(data[indexPath.row].name!)
            //selectedID.append(data[indexPath.row].id!)
            //selectedID.insert(data[indexPath.row].id!, at: indexPath.row)
            data[indexPath.row].is_selected = true
            //KselectedID.append(data[indexPath.row].id!)

            //selectedCell.insert(data[indexPath.row].name!, at: indexPath.row)
            
            
        }
        
        print("selectedCell>>>",selectedCell)
   //     cell.contentView.backgroundColor = .red
        
        listCollectionView.reloadData()
        
        
        
//        var cell = listCollectionView.cellForItem(at: indexPath)
//        if cell?.isSelected == true {
//            cell?.backgroundColor = UIColor.red
//        }else{
//           cell?.backgroundColor = UIColor.black
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
//    func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {
//        if let selectedItems = collectionView.indexPathsForSelectedItems {
//            if selectedItems.contains(indexPath) {
//                collectionView.deselectItem(at: indexPath, animated: true)
//                return false
//            }
//        }
//        return true
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
          print("diddeselect1>>>>")
        
    }
    
    private func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
       
        print("diddeselect>>>>")
        
     //   let cell = collectionView.cellForItem(at: indexPath as IndexPath)!
//        if selectedCell.contains(indexPath as IndexPath) {
//            selectedCell.remove(at: selectedCell.index(of: indexPath as IndexPath)!)
//            cell.contentView.backgroundColor = .white
//        }
        
        
//        var cell = listCollectionView.cellForItem(at: indexPath as IndexPath)
//        cell?.backgroundColor = UIColor.black
//    }
    

}
}



