//
//  RefundHistoryTableViewCell.swift
//  orderAround
//
//  Created by Prem's on 29/09/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class RefundHistoryTableViewCell: UITableViewCell {

    @IBOutlet var invoiceIdLbl: UILabel!
    @IBOutlet var disputeTypeLbl: UILabel!
    @IBOutlet var descriptionLbl: UILabel!
    @IBOutlet var statusLbl: UILabel!
    @IBOutlet var amountLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell() {
        
        Common.setFont(to: invoiceIdLbl, isTitle: true, size: 14, fontType: .semiBold)
        Common.setFont(to: disputeTypeLbl, isTitle: true, size: 14, fontType: .semiBold)
        Common.setFont(to: descriptionLbl, isTitle: true, size: 13, fontType: .regular)
        Common.setFont(to: statusLbl, isTitle: true, size: 13, fontType: .regular)
        Common.setFont(to: amountLbl, isTitle: true, size: 18, fontType: .semiBold)
    }
    
    func updateCell(refundObj: RefundHistoryData) {
        
        invoiceIdLbl.attributedText = setAttributedString(baseString: "Invoice Id: \(refundObj.order?.invoiceID ?? "")", setString: "\(refundObj.order?.invoiceID ?? "")", font: UIFont.Regular(), color: .darkGray)
        
        disputeTypeLbl.attributedText = setAttributedString(baseString: "Dispute Type: \(refundObj.type ?? "")", setString: "\(refundObj.type ?? "")", font: .Regular(), color: .darkGray)//refundObj.type == "CANCELLED" ? .primary : .greenprimary
        
        statusLbl.attributedText = setAttributedString(baseString: "Status: \(refundObj.order?.disputes?.first?.status ?? "")", setString: "\(refundObj.order?.disputes?.first?.status ?? "")", font: .Regular(), color:  refundObj.status == "RESOLVED" ? .greenprimary : .primary)
        
        //descriptionLbl.attributedText = setAttributedString(baseString: "Description: \(refundObj.refundHistoryDescription ?? "")", setString: "\(refundObj.refundHistoryDescription ?? "")", font: .Regular(), color: .darkGray)
        
        descriptionLbl.text = refundObj.refundHistoryDescription ?? ""
        
        amountLbl.text = "$\(refundObj.refundAmount ?? "")"
    }
 
    //statusLbl.attributedText = self.setAttributedString(baseString: "Status : Dispute Created", setString: "Dispute Created", font: UIFont.regular(size: 18), color: .red)
    
    func setAttributedString(baseString: String, setString: String, font: UIFont, color: UIColor) -> NSAttributedString {
        
        let orderBaseString = baseString
        let attributedString = NSMutableAttributedString(string: orderBaseString, attributes: nil)
        let amountRange = (attributedString.string as NSString).range(of: setString)
        attributedString.setAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color], range: amountRange)
        return attributedString
    }
}
