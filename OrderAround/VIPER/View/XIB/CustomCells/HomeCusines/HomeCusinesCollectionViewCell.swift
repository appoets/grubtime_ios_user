//
//  HomeCusinesCollectionViewCell.swift
//  orderAround
//
//  Created by Prem's on 27/11/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class HomeCusinesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateCell(cusineObj: Cuisines) {
        
        let imageName = (cusineObj.name ?? "").removingWhitespaces().lowercased()
        titleLbl.text = cusineObj.name ?? ""
        imageView.image = UIImage(named: "\(imageName)_cus")
        imageView.backgroundColor = UIColor.init(red: 67.0/255.0, green: 77.0/255.0, blue: 107.0/255.0, alpha: 1.0)
    }
    
}
