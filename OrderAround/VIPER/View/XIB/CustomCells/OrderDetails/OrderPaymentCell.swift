//
//  OrderDetailsFooterCell.swift
//  Project
//
//  Created by CSS on 25/10/18.
//  Copyright © 2018 css. All rights reserved.
//

import UIKit

class OrderPaymentCell: UITableViewCell {
    
    
    @IBOutlet weak var labelItemTotal:UILabel!
     @IBOutlet weak var labelTotalValue:UILabel!
    
     @IBOutlet weak var labelTax:UILabel!
     @IBOutlet weak var labelTaxValue:UILabel!
    
        @IBOutlet weak var labelDelivery:UILabel!
    @IBOutlet weak var labelDeliveryValue:UILabel!
    
       @IBOutlet weak var labelDiscount:UILabel!
      @IBOutlet weak var labelDiscountValue:UILabel!
    
    
    @IBOutlet weak var promoDiscount: UILabel!
        @IBOutlet weak var labelPromoValue: UILabel!
    
      @IBOutlet weak var labelWallet:UILabel!
     @IBOutlet weak var labelWalletValue:UILabel!
    
    @IBOutlet weak var labelPayable:UILabel!
    @IBOutlet weak var labelToPayValue:UILabel!
    
    @IBOutlet weak var viewItemTotal:UIView!
    @IBOutlet weak var viewTax:UIView!
    @IBOutlet weak var viewDeliveryCharge:UIView!
    @IBOutlet weak var viewDiscount:UIView!
    @IBOutlet weak var viewWallet:UIView!
    
    @IBOutlet weak var viewPromo: UIView!
    
    
    @IBOutlet weak var totalView: UIView!
    
    
    
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var totalLabelValue: UILabel!
    
    
    @IBOutlet var paidByTitleLbl: UILabel!
    @IBOutlet var paidByCardLbl: UILabel!
    @IBOutlet var paidByOyolaCreditLbl: UILabel!
    @IBOutlet var cardValueLbl: UILabel!
    @IBOutlet var oyolaCreditValueLbl: UILabel!
    
    @IBOutlet var cardStackView: UIStackView!
    @IBOutlet var oyolaCreditStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoads()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPaymentValues(invoice:Invoice) {
        
 //       viewDeliveryCharge.isHidden =  invoice.delivery_charge == 0
//        viewTax.isHidden =  invoice.tax == 0
        viewWallet.isHidden = true //invoice.wallet_amount == 0
        viewPromo.isHidden = invoice.promocode_amount == 0
        labelTaxValue.text = Common.showAmount(amount: invoice.gross ?? 0)
        labelDeliveryValue.text = Common.showAmount(amount: invoice.delivery_charge ?? 0)
        labelWalletValue.text = "-"+Common.showAmount(amount: invoice.wallet_amount ?? 0)
        labelDiscountValue.text = "-"+Common.showAmount(amount: invoice.discount ?? 0)
        labelPromoValue.text = "-"+Common.showAmount(amount: invoice.promocode_amount ?? 0)
        labelToPayValue.text = Common.showAmount(amount: invoice.payable ?? 0)
      //  labelTotalValue.text = Common.showAmount(amount: invoice.gross ?? 0)
        totalLabelValue.text = Common.showAmount(amount: invoice.net ?? 0)
        cardValueLbl.text = Common.showAmount(amount: invoice.payable ?? 0)
        oyolaCreditValueLbl.text = Common.showAmount(amount: invoice.wallet_amount ?? 0)
        //paidByOyolaCreditLbl.isHidden =  invoice.wallet_amount == 0
        viewDeliveryCharge.isHidden =  invoice.delivery_charge == 0
        cardStackView.isHidden = invoice.payable == 0
        oyolaCreditStackView.isHidden = invoice.wallet_amount == 0
        viewDiscount.isHidden =  invoice.discount == 0
    }
    
}

//MARK: - Methods

extension OrderPaymentCell {
    private func initialLoads() {
        
        Common.setFont(to: labelItemTotal, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelTax, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelDelivery, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelDiscount, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelWallet, isTitle: false, size: 14, fontType: .regular)
        
        Common.setFont(to: labelTotalValue, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelTaxValue, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelDeliveryValue, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelDiscountValue, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelWalletValue, isTitle: false, size: 14, fontType: .regular)
        
        Common.setFont(to: labelPayable, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelToPayValue, isTitle: false, size: 14, fontType: .regular)
         Common.setFont(to: totalLabel, isTitle: false, size: 14, fontType: .regular)
         Common.setFont(to: totalLabelValue, isTitle: false, size: 14, fontType: .regular)
//        labelTax.textColor = .greenColor
        Common.setFont(to: promoDiscount, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: labelPromoValue, isTitle: false, size: 14, fontType: .regular)
        localize()
        Common.setFont(to: paidByTitleLbl, isTitle: false, size: 14, fontType: .regular)
        localize()
        Common.setFont(to: paidByCardLbl, isTitle: false, size: 14, fontType: .regular)
        localize()
        Common.setFont(to: paidByOyolaCreditLbl, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: cardValueLbl, isTitle: false, size: 14, fontType: .regular)
        Common.setFont(to: oyolaCreditValueLbl, isTitle: false, size: 14, fontType: .regular)
        localize()
    }
    
    private func localize() {
//        labelTax.backgroundColor = UIColor.black
//        labelDelivery.tintColor = UIColor.black
        labelDiscount.tintColor = UIColor.red
        
       // labelItemTotal.text = APPLocalize.localizestring.cartTotalItem.localize()
        labelTax.text = "Subtotal"//APPLocalize.localizestring.totalItem.localize()
        labelDelivery.text = APPLocalize.localizestring.deliveryFee.localize()
        labelDiscount.text = APPLocalize.localizestring.discount.localize()
        totalLabel.text = "Total"
        labelWallet.text = "GrubTime credits"
        labelPayable.text = APPLocalize.localizestring.paid.localize()
        paidByTitleLbl.text = APPLocalize.localizestring.paidBy.localize()
        paidByCardLbl.text = APPLocalize.localizestring.cardLowerCase.localize()
        paidByOyolaCreditLbl.text = APPLocalize.localizestring.oyolaCredits.localize()
    }
}
