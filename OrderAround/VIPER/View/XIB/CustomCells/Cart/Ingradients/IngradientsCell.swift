//
//  IngradientsCell.swift
//  orderAround
//
//  Created by Chan Basha on 23/12/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit

class IngradientsCell: UITableViewCell {

    @IBOutlet weak var ingradientTextView: UITextView!
    @IBOutlet weak var labelIngradients: UILabel!
    
    
    @IBOutlet weak var labelCalories: UILabel!
    
    @IBOutlet weak var caloriesValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Common.setFont(to: ingradientTextView, isTitle: false, size: 12, fontType: .semiBold)
        Common.setFont(to: labelIngradients, isTitle: true, size: 15, fontType: .semiBold)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
