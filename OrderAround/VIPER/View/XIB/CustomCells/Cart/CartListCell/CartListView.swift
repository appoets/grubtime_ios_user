//
//  CartListView.swift
//  Project
//
//  Created by CSS on 16/10/18.
//  Copyright © 2018 css. All rights reserved.
//

import UIKit

protocol CartListViewDelegate: class{
    
    func addItem(indexPath: IndexPath)
    func removeItem(indexPath: IndexPath)
}

class CartListView: UITableViewCell {

    @IBOutlet weak var customizedlabel: UILabel!
    @IBOutlet weak var customizedButton: UIButton!
    @IBOutlet weak var addOnsLabel: UILabel!
    @IBOutlet weak var cartCount: UILabel!
    @IBOutlet weak var addToCart: UIButton!
    @IBOutlet weak var removeFromCart: UIButton!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet var addonPriceLbl: UILabel!
    @IBOutlet weak var itemNameLbl: UILabel!
    @IBOutlet weak var vegOrNonVeg: UIImageView!
    @IBOutlet weak var customizedView: UIView!
    var indexPath: IndexPath?
    weak var delegate: CartListViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
   
        
        Common.setFont(to: cartCount, isTitle: true, size: 12, fontType: .regular)
        
          Common.setFont(to: itemNameLbl, isTitle: true, size: 12, fontType: .semiBold)
        
           Common.setFont(to: priceLbl, isTitle: true, size: 12, fontType: .semiBold)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(indexPath: IndexPath) {
        
        self.indexPath = indexPath
    }
    
    @IBAction func removeItemAction(_ sender: Any) {
        
        guard indexPath != nil else {return}
        delegate?.removeItem(indexPath: self.indexPath!)
    }
    
    @IBAction func addCartAction(_ sender: Any) {
        
        guard indexPath != nil else {return}
        delegate?.addItem(indexPath: self.indexPath!)
    }
    
}
