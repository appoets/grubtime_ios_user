//
//  HomeDeliveryTypeTableViewCell.swift
//  orderAround
//
//  Created by Prem's on 18/11/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class HomeDeliveryTypeTableViewCell: UITableViewCell {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var selectCheckBoxImgView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        selectCheckBoxImgView.image = selected ? UIImage(named: "cellSelect") : UIImage(named: "cellUnselect")
    }
    
}
