//
//  HomeSortTableViewCell.swift
//  orderAround
//
//  Created by Prem's on 10/11/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

enum SortViewType{
    case home
    case kitchen
}

enum SortType: Int {
    case deliveryType
    case rating
    case pickup
    case reset
    
    var description: String {
        get {
            switch self {
            case .deliveryType:
                return "Delivery Type"
            case .rating:
                return "Rating"
            case .pickup:
                return "Pickup"
            case .reset:
                return "Reset"
            }
        }
    }
}

protocol HomeSortTableViewCellDelegate: class {
    
    func getSelected(index: Int)
}

class HomeSortTableViewCell: UITableViewCell {

    @IBOutlet var sortLbl: UILabel!
    @IBOutlet var sortCollectionView: UICollectionView!
    weak var delegate: HomeSortTableViewCellDelegate?
    var viewType: SortViewType = .home
    
    var sortArray: [String] = [SortType.deliveryType.description, SortType.rating.description, SortType.pickup.description]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sortCollectionView.register(HomeSortCollectionViewCell.nib, forCellWithReuseIdentifier: HomeSortCollectionViewCell.cellIdentifier)
        sortCollectionView.delegate = self
        sortCollectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func updateCell(titleArray: [String] = [SortType.deliveryType.description, SortType.rating.description, SortType.pickup.description], selectedIndex: Int?) {
        
        sortArray = titleArray
        sortArray.insert("Reset", at: 3)
        sortCollectionView.reloadData()
        if selectedIndex != nil{
            sortCollectionView.selectItem(at: IndexPath(item: selectedIndex ?? 0, section: 0), animated: false, scrollPosition: .left)
        }
    }
    
    func updateCellForKitchen(titleArray: [String], selectedIndex: Int) {
        viewType = .kitchen
        sortArray = titleArray
        sortArray.insert("All", at: 0)
        sortCollectionView.reloadData()
        sortCollectionView.selectItem(at: IndexPath(item: selectedIndex, section: 0), animated: false, scrollPosition: .left)
    }
}

extension HomeSortTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return sortArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let sortCell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeSortCollectionViewCell.cellIdentifier, for: indexPath) as! HomeSortCollectionViewCell
        if self.viewType == .home{
            sortCell.updateCell(title: sortArray[indexPath.row], isArrowType: indexPath.row < 2)
        }else if self.viewType == .kitchen{
            sortCell.updateCell(title: sortArray[indexPath.row], isArrowType: false)
        }
        return sortCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        delegate?.getSelected(index: indexPath.row)
    }
}

extension HomeSortTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let cell = Bundle.main.loadNibNamed(HomeSortCollectionViewCell.cellIdentifier, owner: self, options: nil)?.first as? HomeSortCollectionViewCell else {
            return CGSize.zero
        }
        cell.titleLbl.text = sortArray[indexPath.row]
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let size: CGSize = cell.contentView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        return CGSize(width: size.width + 10, height: 50)
    }
}
 
