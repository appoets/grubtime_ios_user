//
//  HomeSortCollectionViewCell.swift
//  orderAround
//
//  Created by Prem's on 10/11/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class HomeSortCollectionViewCell: UICollectionViewCell {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var spacingLbl: UILabel!
    @IBOutlet var downArrowImgView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var titleLblMainViewTrailing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainView.borderColor = UIColor.primary
        mainView.borderLineWidth = 1.0
        mainView.clipsToBounds = true
        downArrowImgView.imageTintColor(color1: UIColor.primary)
    }

    override var isSelected: Bool {
        didSet {
            
            mainView.backgroundColor = isSelected ? UIColor.primary : UIColor.white
            titleLbl.textColor = isSelected ? UIColor.white : UIColor.primary
            spacingLbl.backgroundColor = isSelected ? UIColor.white : UIColor.primary
            downArrowImgView.setImageColor(color: isSelected ? UIColor.white : UIColor.primary)
            
        }
    }
    
    func updateCell(title: String, isArrowType: Bool) {
        
        downArrowImgView.isHidden = !isArrowType
        spacingLbl.isHidden = !isArrowType
        titleLblMainViewTrailing.isActive = !isArrowType
        titleLbl.text = title
    }
}
