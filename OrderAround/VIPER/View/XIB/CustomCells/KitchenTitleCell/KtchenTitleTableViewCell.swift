//
//  KtchenTitleTableViewCell.swift
//  orderAround
//
//  Created by Prem's on 24/10/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

class KtchenTitleTableViewCell: UITableViewCell {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var descriptionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }

    func setupUI() {
        
        titleLbl.textColor = #colorLiteral(red: 0.8376379609, green: 0.1570017338, blue: 0.1604415178, alpha: 1)
        Common.setFont(to: titleLbl, isTitle: true, size: 20, fontType: .bold)
        descriptionLbl.textColor = .darkGray
        Common.setFont(to: descriptionLbl, isTitle: true, size: 12, fontType: .semiBold)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateCell(shopObj: Shops) {
        
        titleLbl.text = shopObj.name
        descriptionLbl.text = shopObj.description
    }
}
