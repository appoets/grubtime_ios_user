//
//  ResturantRatingCell.swift
//  Project
//
//  Created by CSS on 22/10/18.
//  Copyright © 2018 css. All rights reserved.
//

import UIKit

class ResturantRatingCell: UITableViewCell {

    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelRatingCount: UILabel!
    @IBOutlet weak var labelDelivery: UILabel!
    @IBOutlet weak var labelOfferPercentage: UILabel!
    @IBOutlet weak var imageOffer: UIImageView!
    @IBOutlet weak var labelRating: UILabel!
    
    @IBOutlet weak var categoryCV: UICollectionView!
    
    @IBOutlet weak var labelHalal: UILabel!
    var categoryArray = [String]()
    @IBOutlet weak var offerImage: UIImageView!
    @IBOutlet weak var labelOffer: UILabel!
    @IBOutlet var freeDeliveryLbl: UILabel!
    @IBOutlet weak var freeDeliveryImageView: UIImageView!
    
    
    var categoriesListArray = [Categories]()
    var featuredProducts = [Featured_products]()


    var selectedCategory :((Categories?,[Featured_products]?)->Void)?
    var selectedAll :(([Categories]?,[Featured_products]?)->Void)?
    
    var selectedName = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoads()
        
//        self.categoryArray.append(categoriesListArray[0]!)
//        let selectedIndexPath = IndexPath(item: 0, section: 0)
//        categoryCV.selectItem(at: selectedIndexPath, animated: false, scrollPosition: .left)
        
        
    }
    
    
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension ResturantRatingCell {
    
    private func initialLoads() {
        
        Common.setFont(to: labelTime, isTitle: false, size: 17, fontType: .regular)
        Common.setFont(to: labelRatingCount, isTitle: false, size: 14, fontType: .light)
        Common.setFont(to: labelDelivery, isTitle: false, size: 15, fontType: .light)
        Common.setFont(to: labelOfferPercentage, isTitle: false, size: 12, fontType: .regular)
        Common.setFont(to: labelRating, isTitle: false, size: 14, fontType: .regular)
         Common.setFont(to: labelHalal, isTitle: false, size: 14, fontType: .bold)
         Common.setFont(to: labelOffer, isTitle: false, size: 14, fontType: .semiBold)
        Common.setFont(to: freeDeliveryLbl, isTitle: false, size: 14, fontType: .semiBold)
        imageOffer.imageTintColor(color1: .red)
        labelOfferPercentage.textColor = .red
        labelDelivery.textColor = .lightGray
        
        categoryCV.delegate = self
        categoryCV.dataSource  = self
        self.categoryCV.reloadData()
        categoryCV.register(UINib(nibName: XIB.Names.CategoryMenu, bundle: nil), forCellWithReuseIdentifier: XIB.Names.CategoryMenu)
    }
    
}
extension ResturantRatingCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.categoriesListArray.count + 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //cell.buttonCategory.backgroundColor = UIColor.primary
        
        
        
        if indexPath.row == 0 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:XIB.Names.CategoryMenu, for: indexPath) as!  CategoryMenu
            cell.buttonCategory.titleLabel?.textColor = UIColor.white
           
            cell.buttonCategory.backgroundColor = #colorLiteral(red: 0.7725490196, green: 0.231372549, blue: 0.2039215686, alpha: 1)
            cell.buttonCategory.setTitle("All", for: .normal)
            
            cell.buttonCategory.addTarget(self, action: #selector(seletedAll(sender:)), for: .touchUpInside)
            cell.buttonCategory.layer.cornerRadius = 10
            return cell
            
        }else{
        
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier:XIB.Names.CategoryMenu, for: indexPath) as!  CategoryMenu
     
        if self.selectedName.contains(self.categoriesListArray[indexPath.row - 1].name!) {
            
            cell.buttonCategory.setTitle(self.categoriesListArray[indexPath.row - 1].name, for: .normal)
            cell.buttonCategory.titleLabel?.textColor = UIColor.white
            cell.buttonCategory.backgroundColor = #colorLiteral(red: 0.7725490196, green: 0.231372549, blue: 0.2039215686, alpha: 1)
            cell.buttonCategory.layer.cornerRadius = 10
            cell.buttonCategory.tag = indexPath.row - 1
            cell.buttonCategory.addTarget(self, action: #selector(selectCategory(sender:)), for: .touchUpInside)
            return cell
            
        
            
        }else
        {
            cell.buttonCategory.setTitle(self.categoriesListArray[indexPath.row - 1].name, for: .normal)
            cell.buttonCategory.layer.cornerRadius = 10
            cell.buttonCategory.backgroundColor =   #colorLiteral(red: 0.7725490196, green: 0.231372549, blue: 0.2039215686, alpha: 1) // UIColor.lightGray
            cell.buttonCategory.titleLabel?.textColor = UIColor.white
            cell.buttonCategory.tag = indexPath.row - 1
            cell.buttonCategory.addTarget(self, action: #selector(selectCategory(sender:)), for: .touchUpInside)
            return cell
            
        }
        
              
            
            
        }
        
       // return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       //selectedID?(indexPath.row)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        
        
        if indexPath.row == 0 {
            
            let label = UILabel(frame: CGRect.zero)
            label.text = "ALL"
            label.sizeToFit()
            return CGSize(width: label.frame.width + 15, height: 60)
            
        }else{
            let label = UILabel(frame: CGRect.zero)
                   label.text = self.categoriesListArray[indexPath.item - 1].name
                   label.sizeToFit()
                   return CGSize(width: label.frame.width + 15, height: 60)
        }
        
        
        
        
    }

    
    @IBAction func selectCategory(sender:UIButton){
        selectedName.removeAll()
        selectedCategory?(self.categoriesListArray[sender.tag],featuredProducts)
        selectedName.append(self.categoriesListArray[sender.tag].name!)
        categoryCV.reloadData()

    }
    
    
    @IBAction func seletedAll(sender:UIButton){
        
        selectedAll?(self.categoriesListArray,featuredProducts)
    
    }

    
}
