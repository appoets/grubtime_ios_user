//
//  CollectionTableCell.swift
//  Project
//
//  Created by CSS on 15/10/18.
//  Copyright © 2018 css. All rights reserved.
//

import UIKit

enum CollectionTableCellType {
    case banner
    case suggestion
}

protocol HomeBannerDelegate: class {
    func didSelectBanner(bannerObj: Banners)
    func didSelectCusines(index: Int)
}

class CollectionTableCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var bannerArray: [Banners] = []
    weak var delegate: HomeBannerDelegate?
    var viewType: CollectionTableCellType = .banner
    var cusinesArray: [Cuisines] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let nibPost = UINib(nibName: XIB.Names.TrendingCell, bundle: nil)
        collectionView.register(nibPost, forCellWithReuseIdentifier: XIB.Names.TrendingCell)
        collectionView.register(HomeCusinesCollectionViewCell.nib, forCellWithReuseIdentifier: HomeCusinesCollectionViewCell.cellIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        layer.cornerRadius = 10
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        collectionView.alwaysBounceHorizontal = false
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 5, bottom:10, right: 5)
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateCell(bannerArray: [Banners]){
        
        self.bannerArray = bannerArray
        self.collectionView.reloadData()
    }
    
    func updateSuggestionCell(cusinesArray: [Cuisines]) {
        
        self.cusinesArray = cusinesArray
        self.collectionView.reloadData()
    }
    
}

extension CollectionTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if viewType == .banner{
            return self.bannerArray.count
        }else{
            return self.cusinesArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if viewType == .banner{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: XIB.Names.TrendingCell, for: indexPath) as! TrendingCell
            if bannerArray.count > 0 && bannerArray.count > indexPath.row {
                let bannerObj = bannerArray[indexPath.row]
                cell.trendingProduct.setImage(with: bannerObj.shop?.avatar, placeHolder: #imageLiteral(resourceName: "product_placeholder"))
                cell.layer.cornerRadius = 10
                
                cell.estimatedTimeLbl.text = "\(bannerArray[indexPath.row].shop?.estimated_delivery_time ?? 0) Mins"
                cell.kitchenLbl.text = "\(bannerArray[indexPath.row].shop?.name ?? "")"
                if let offerType = bannerArray[indexPath.row].shop?.offer_type{
                    if offerType == "PERCENTAGE" {
                        cell.offerLbl.text = "\(bannerArray[indexPath.row].shop?.offer_percent ?? 0)% OFF"
                    }else{
                        cell.offerLbl.text = "\(bannerArray[indexPath.row].shop?.offer_percent ?? 0)$ OFF"
                    }
                }
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCusinesCollectionViewCell.cellIdentifier, for: indexPath) as! HomeCusinesCollectionViewCell
            cell.updateCell(cusineObj: self.cusinesArray[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if viewType == .banner{
            delegate?.didSelectBanner(bannerObj: bannerArray[indexPath.row])
        }else{
            delegate?.didSelectCusines(index: indexPath.row)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if viewType == .banner{
            let cellWidth = (collectionView.frame.width/3)
            return CGSize(width: CGFloat(cellWidth), height: collectionView.frame.height)
        }else{
            let cellWidth = (collectionView.frame.width/3)
            return CGSize(width: CGFloat(cellWidth), height: collectionView.frame.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        return UIEdgeInsetsMake(10, 5, 10, 5)
    }
}
