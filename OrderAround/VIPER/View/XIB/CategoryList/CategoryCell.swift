//
//  CategoryCell.swift
//  orderAround
//
//  Created by Chan Basha Shaik on 16/12/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    
    @IBOutlet weak var categoryCV: UICollectionView!
    
    var categoryArray = [String]()
    
    
    var selectedID :((Int?)->Void)?
    
    override func awakeFromNib() {
        
        
        
        categoryCV.delegate = self
        categoryCV.dataSource  = self
        
        
    }
    

    
}
extension CategoryCell : UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
     return self.categoryArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
     let cell = collectionView.dequeueReusableCell(withReuseIdentifier:XIB.Names.CategoryMenu, for: indexPath) as!  CategoryMenu
    
        return cell
 
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedID?(indexPath.row)
        
  
        
    }

    
}
