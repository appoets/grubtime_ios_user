//
//  CategoryMenu.swift
//  orderAround
//
//  Created by Chan Basha Shaik on 16/12/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit

class CategoryMenu: UICollectionViewCell {
    
    
    @IBOutlet weak var buttonCategory: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        Common.setFont(to: buttonCategory, isTitle: true, size: 14, fontType: .semiBold)
        buttonCategory.titleLabel?.textColor = UIColor.black
    }

}
