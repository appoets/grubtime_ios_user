//
//  FavouriteCollectionTableViewCell.swift
//  orderAround
//
//  Created by Chan Basha Shaik on 21/11/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit


protocol  FavouriteTableViewCellDelegate: class {
    
    func getSelectedShop(collectionType: HomeCollectionViewType, index: Int)
}

enum HomeCollectionViewType: Int {
    case favourite
    case free
    case partyCatering
    case mealService
}

class FavouriteCollectionTableViewCell: UITableViewCell {

    @IBOutlet weak var favCollectionview: UICollectionView!
    
    var freeDeliveryShops: [Shops] = []
    var favShops: [Shops] = []
    var commonShops: [Shops] = []
    var collectinViewType: HomeCollectionViewType = .favourite
    weak var delegate: FavouriteTableViewCellDelegate?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        
        let nibPost = UINib(nibName: XIB.Names.FavCuisineCollectionViewCell, bundle: nil)
        favCollectionview.register(nibPost, forCellWithReuseIdentifier: XIB.Names.FavCuisineCollectionViewCell)
        favCollectionview.delegate = self
        favCollectionview.dataSource = self
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        favCollectionview.collectionViewLayout = layout
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        favCollectionview.alwaysBounceHorizontal = false
        favCollectionview.contentInset = UIEdgeInsets(top: 10, left: 5, bottom:10, right: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCellforFreeDelivery(collectionViewType: HomeCollectionViewType, shopsArray: [Shops], isFilterApplied: Bool) {
        
        if !isAppliedFilter(){
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            favCollectionview.collectionViewLayout = layout
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            favCollectionview.alwaysBounceHorizontal = false
            favCollectionview.contentInset = UIEdgeInsets(top: 10, left: 5, bottom:10, right: 5)
        }
        self.collectinViewType = collectionViewType
        self.freeDeliveryShops = shopsArray
        self.favCollectionview.reloadData()
    }
    
    
    func updateCellForFavShops(collectionViewType: HomeCollectionViewType, shopsArray: [Shops], isFilterApplied: Bool) {
        
        if !isAppliedFilter(){
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            favCollectionview.collectionViewLayout = layout
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            favCollectionview.alwaysBounceHorizontal = false
            favCollectionview.contentInset = UIEdgeInsets(top: 10, left: 5, bottom:10, right: 5)
        }
        self.collectinViewType = collectionViewType
        self.favShops = shopsArray
        self.favCollectionview.reloadData()
    }
    
    func updateCellForCommon(collectionViewType: HomeCollectionViewType, shopsArray: [Shops], isFilterApplied: Bool) {
        
        if !isAppliedFilter(){
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            favCollectionview.collectionViewLayout = layout
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            favCollectionview.alwaysBounceHorizontal = false
            favCollectionview.contentInset = UIEdgeInsets(top: 10, left: 5, bottom:10, right: 5)
        }
        self.collectinViewType = collectionViewType
        self.commonShops = shopsArray
        self.favCollectionview.reloadData()
    }
}

extension FavouriteCollectionTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        
        return UIEdgeInsetsMake(10, 5, 10, 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = collectionView.frame.width
        return CGSize(width: cellWidth - 30 , height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectinViewType == .favourite{
            return self.favShops.count
        }else if collectinViewType == .free{
            return self.freeDeliveryShops.count
        }else{
            return self.commonShops.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectinViewType == .favourite{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: XIB.Names.FavCuisineCollectionViewCell, for: indexPath) as! FavCuisineCollectionViewCell
            let bannerEntity = self.favShops[indexPath.row]
            cell.favImageView.setImage(with: bannerEntity.avatar, placeHolder: #imageLiteral(resourceName: "product_placeholder"))
            cell.lblCuisineName.text = self.favShops[indexPath.row].name
            //cell.lblRating.text = self.favShops[indexPath.row].description
            cell.lblRating.text = self.favShops[indexPath.row].cuisines?.compactMap{$0.name}.joined(separator: ", ")
            
            cell.closedLbl.isHidden = self.favShops[indexPath.row].shopstatus == SHOPSTATUS().open ? true : false
            if self.favShops[indexPath.row].halal == 1{
                cell.lblSubCuisines.text = "Halal"
                //cell.halalHeight.constant = 23
                cell.lblSubCuisines.textColor = .green
            }else{
                cell.lblSubCuisines.text = ""
                //cell.lblSubCuisines.isHidden = true
                //cell.halalHeight.constant = 0
            }
            
            if let offer = self.favShops[indexPath.row].offer_percent {
                
                cell.imageOffer.image = UIImage(named: "offer_icon")
                cell.imageOffer.imageTintColor(color1: .red)
                cell.labeloffer.textColor = .red
                cell.labeloffer.isHidden = false
                cell.imageOffer.isHidden = false
                
                if offer == 0{
                    cell.labeloffer.isHidden = true
                    cell.imageOffer.isHidden = true
                    cell.offerImageHeight.constant = 0
                    //cell.offerLabelHeight.constant = 0
                }else{
                    let offerMinAmout = "\(self.favShops[indexPath.row].offer_min_amount ?? 0)"
                    cell.labeloffer.text = APPLocalize.localizestring.get.localize()+"\(offer)"+APPLocalize.localizestring.offOrder.localize() + offerMinAmout
                    cell.offerImageHeight.constant = 20
                    //cell.offerLabelHeight.constant = 30
                }
            }else
            {
                cell.labeloffer.isHidden = true
                cell.imageOffer.isHidden = true
            }
            
            if self.favShops[indexPath.row].customer_rating != 0 {
                cell.labelRating.text = "\(self.favShops[indexPath.row].customer_rating ?? 5)"//.0"//+APPLocalize.localizestring.Rating.localize()
                cell.labelRating.textColor = .gray
            }else{
                cell.labelRating.text = "5" //"0.0"//APPLocalize.localizestring.noRating.localize()
            }
            cell.lblDeliveryTime.text = "\(self.favShops[indexPath.row].estimated_delivery_time ?? 0) Mins"
            Common.setFont(to: cell.lblDeliveryTime, isTitle: true, size: 14, fontType: .semiBold)
            
            // }
            
            cell.favImageView.layer.cornerRadius = 10
            return cell
            
        }else if collectinViewType == .free{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: XIB.Names.FavCuisineCollectionViewCell, for: indexPath) as! FavCuisineCollectionViewCell
            let bannerEntity = self.freeDeliveryShops[indexPath.row]
            cell.favImageView.setImage(with: bannerEntity.avatar, placeHolder: #imageLiteral(resourceName: "product_placeholder"))
            cell.lblCuisineName.text = bannerEntity.name
            //cell.lblRating.text = bannerEntity.description
            cell.lblRating.text = self.freeDeliveryShops[indexPath.row].cuisines?.compactMap{$0.name}.joined(separator: ", ")
            cell.closedLbl.isHidden = self.freeDeliveryShops[indexPath.row].shopstatus == SHOPSTATUS().open ? true : false
            if bannerEntity.halal == 1{
                cell.lblSubCuisines.text = "Halal"
                //cell.halalHeight.constant = 20
                cell.lblSubCuisines.textColor = .green
            }else{
                cell.lblSubCuisines.text = ""
                //cell.lblSubCuisines.isHidden = true
                //cell.halalHeight.constant = 0
            }
            if let offer = bannerEntity.offer_percent {
                cell.imageOffer.image = UIImage(named: "offer_icon")
                cell.imageOffer.imageTintColor(color1: .red)
                cell.labeloffer.textColor = .red
                cell.labeloffer.isHidden = false
                cell.imageOffer.isHidden = false
                if offer == 0{
                    cell.labeloffer.isHidden = true
                    cell.imageOffer.isHidden = true
                    cell.offerImageHeight.constant = 0
                    //cell.offerLabelHeight.constant = 0
                }else{
                    cell.labeloffer.text = APPLocalize.localizestring.get.localize()+"\(offer)"+APPLocalize.localizestring.offOrder.localize()+"\(bannerEntity.offer_min_amount ?? 10)"
                    cell.offerImageHeight.constant = 20
                    //cell.offerLabelHeight.constant = 30
                }
            }else
            {
                cell.labeloffer.isHidden = true
                cell.imageOffer.isHidden = true
            }
            
            if bannerEntity.customer_rating != 0{
                cell.labelRating.text = "\(self.freeDeliveryShops[indexPath.row].customer_rating ?? 5)" //".0"//+APPLocalize.localizestring.Rating.localize()
                cell.labelRating.textColor = .gray
            }else{
                cell.labelRating.text = "5" //"0.0"//APPLocalize.localizestring.noRating.localize()
                cell.labelRating.textColor = .gray
            }
            cell.lblDeliveryTime.text = "\(self.freeDeliveryShops[indexPath.row].estimated_delivery_time ?? 0) Mins"
            Common.setFont(to: cell.lblDeliveryTime, isTitle: true, size: 14, fontType: .semiBold)
            cell.favImageView.layer.cornerRadius = 10
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: XIB.Names.FavCuisineCollectionViewCell, for: indexPath) as! FavCuisineCollectionViewCell
            let bannerEntity = self.commonShops[indexPath.row]
            cell.favImageView.setImage(with: bannerEntity.avatar, placeHolder: #imageLiteral(resourceName: "product_placeholder"))
            cell.lblCuisineName.text = bannerEntity.name
            //cell.lblRating.text = bannerEntity.description
            cell.lblRating.text = self.commonShops[indexPath.row].cuisines?.compactMap{$0.name}.joined(separator: ", ")
            cell.closedLbl.isHidden = self.commonShops[indexPath.row].shopstatus == SHOPSTATUS().open ? true : false
            if bannerEntity.halal == 1{
                cell.lblSubCuisines.text = "Halal"
                //cell.halalHeight.constant = 20
                cell.lblSubCuisines.textColor = .green
            }else{
                cell.lblSubCuisines.text = ""
                //cell.lblSubCuisines.isHidden = true
                //cell.halalHeight.constant = 0
            }
            if let offer = bannerEntity.offer_percent {
                cell.imageOffer.image = UIImage(named: "offer_icon")
                cell.imageOffer.imageTintColor(color1: .red)
                cell.labeloffer.textColor = .red
                cell.labeloffer.isHidden = false
                cell.imageOffer.isHidden = false
                if offer == 0{
                    cell.labeloffer.isHidden = true
                    cell.imageOffer.isHidden = true
                    cell.offerImageHeight.constant = 0
                    //cell.offerLabelHeight.constant = 0
                }else{
                    cell.labeloffer.text = APPLocalize.localizestring.get.localize()+"\(offer)"+APPLocalize.localizestring.offOrder.localize()+"\(bannerEntity.offer_min_amount ?? 10)"
                    cell.offerImageHeight.constant = 20
                    //cell.offerLabelHeight.constant = 30
                }
            }else
            {
                cell.labeloffer.isHidden = true
                cell.imageOffer.isHidden = true
            }
            
            if bannerEntity.customer_rating != 0{
                cell.labelRating.text = "\(self.commonShops[indexPath.row].customer_rating ?? 5)" //".0"//+APPLocalize.localizestring.Rating.localize()
                cell.labelRating.textColor = .gray
            }else{
                cell.labelRating.text = "5" //"0.0"//APPLocalize.localizestring.noRating.localize()
                cell.labelRating.textColor = .gray
            }
            cell.lblDeliveryTime.text = "\(self.commonShops[indexPath.row].estimated_delivery_time ?? 0) Mins"
            Common.setFont(to: cell.lblDeliveryTime, isTitle: true, size: 14, fontType: .semiBold)
            cell.favImageView.layer.cornerRadius = 10
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        self.delegate?.getSelectedShop(collectionType: self.collectinViewType, index: indexPath.row)
    }
    
}
