//
//  RatingRangeViewController.swift
//  orderAround
//
//  Created by Prem's on 12/11/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

protocol RatingRangeViewDelegate: class {
    func getSliderValue(value: Int)
}

class RatingRangeViewController: UIViewController {
    
    @IBOutlet var ratingValueLbl: UILabel!
    @IBOutlet var valueSlider: UISlider!
    var numbers = [0, 1, 2, 3, 4, 5]
    var oldIndex = 0
    var applyButtonClicked: ((_ value: Double) -> Void)?
    var newValue = 0
    weak var delegate: RatingRangeViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let numberOfSteps = Float(numbers.count - 1)
        valueSlider.maximumValue = numberOfSteps;
        valueSlider.minimumValue = 0;
    }

    
    @IBAction func sliderAction(_ sender: UISlider) {
        
        let index = (Int)(valueSlider.value);
        valueSlider.setValue(Float(index), animated: false)
        let number = numbers[index]
        if oldIndex != index{
            print("sliderIndex:\(index)")
            print("number: \(number)")
            oldIndex = index
            ratingValueLbl.text = "\(number)"
            newValue = number
        }
    }
    
    
    @IBAction func applyBtnAction(_ sender: Any) {
        
        self.dismiss(animated: true) {
            self.delegate?.getSliderValue(value: self.newValue)
        }
    }
    
}
