//
//  SortDeliveryTypeViewController.swift
//  orderAround
//
//  Created by Prem's on 12/11/20.
//  Copyright © 2020 css. All rights reserved.
//

import UIKit

protocol SortDeliveryDelegate: class {
    
    func getSelectedType(type: Int)
}

class SortDeliveryTypeViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    let titleArray: [String] = ["Delivery fee", "Free delivery"]
    weak var delegate: SortDeliveryDelegate?
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(HomeDeliveryTypeTableViewCell.nib, forCellReuseIdentifier: HomeDeliveryTypeTableViewCell.cellIdentifier)
    }

    @IBAction func applyBtnAction(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.getSelectedType(type: self.selectedIndex)
        }
    }
}

extension SortDeliveryTypeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeDeliveryTypeTableViewCell.cellIdentifier) as! HomeDeliveryTypeTableViewCell
        cell.titleLbl.text = titleArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.row
    }
    
}
