//
//  SelectPickUpType.swift
//  orderAround
//
//  Created by Chan Basha Shaik on 31/10/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit
import LFTimePicker

class SelectPickUpType: UIView {

    @IBOutlet weak var labelSelect: UILabel!
    
    @IBOutlet weak var dateSelector: UIDatePicker!
    
    @IBOutlet weak var buttonCancel: UIButton!
    
    
    @IBOutlet weak var buttonProcceed: UIButton!
    
    @IBOutlet weak var buttonSchedule: UIButton!
    
    
    @IBOutlet weak var buttonASAP: UIButton!
   
    
    @IBOutlet weak var labelASAP: UILabel!
    
    @IBOutlet weak var labelSchedule: UILabel!
    
    
    var isASAP = false
    var isSceduled = false
    var scheduleTime : ((String?)->Void)?
    var asap : ((Bool?)->Void)?
    var cancel :((Bool?)->Void)?
    var tapOn : ((Bool?)->Void)?

    var dateAndTime = ""
    var dataeSelector = LFTimePickerController()
    
    
    @IBOutlet weak var labelDate: UILabel!
    
    override func awakeFromNib() {
        
        dateSelector.datePickerMode = UIDatePickerMode.dateAndTime
        let currentDate = Date()
        dateSelector.minimumDate = Calendar.current.date(byAdding: .minute, value: 125, to: Date())
        dateSelector.maximumDate = Calendar.current.date(byAdding: .weekOfYear, value: 2, to: Date())
        //dateSelector.date = currentDate
        self.buttonSchedule.addTarget(self, action: #selector(schedule(sender:)), for: .touchUpInside)
        self.buttonASAP.addTarget(self, action: #selector(schedule(sender:)), for: .touchUpInside)
        self.buttonProcceed.addTarget(self, action: #selector(procceed(sender:)), for: .touchUpInside)
        self.buttonCancel.addTarget(self, action: #selector(cancel(sender:)), for: .touchUpInside)
        setFont()
        buttonCancel.layer.cornerRadius = 8
        buttonProcceed.layer.cornerRadius = 8
        
    }
    
    
    
    func setFont(){
        
        Common.setFont(to: labelSelect, isTitle: true, size: 16, fontType: .semiBold)
        Common.setFont(to: labelASAP , isTitle: true, size: 14, fontType: .semiBold)
        Common.setFont(to: labelSchedule, isTitle: true, size: 14, fontType: .semiBold)
        Common.setFont(to: labelDate, isTitle: true, size: 14, fontType: .semiBold)
        Common.setFont(to: buttonProcceed, isTitle: true, size: 17, fontType: .semiBold)
        Common.setFont(to: buttonCancel, isTitle: true, size: 17, fontType: .semiBold)

        self.labelDate.textColor = .primary
        
    }
    
    @IBAction func schedule(sender:UIButton)
        
    {
        if sender.tag == 1
        {
            isASAP = !isASAP
            self.buttonASAP.setImage(isASAP ? #imageLiteral(resourceName: "cellSelect") : #imageLiteral(resourceName: "FilterUnSelectedIcon"), for: .normal)
            self.dateSelector.isHidden = true
            self.buttonSchedule.setImage(#imageLiteral(resourceName: "cellUnselect"), for: .normal)
            self.labelDate.text = ""
            
        }else if sender.tag == 2
        {
            isSceduled = !isSceduled
            self.buttonSchedule.setImage(isSceduled ? #imageLiteral(resourceName: "cellSelect") : #imageLiteral(resourceName: "FilterUnSelectedIcon"), for: .normal)
            self.buttonASAP.setImage(#imageLiteral(resourceName: "cellUnselect"), for: .normal)
           // self.dateSelector.isHidden = false
            
            tapOn?(true)
            
        }
    }
   
    @IBAction func procceed(sender:UIButton)
        
    {
        dateAndTime = "\(self.dateSelector.date)"
        
        if isASAP == false && isSceduled == false {
            self.makeToast("Please select order type")
            return
        }
      
        if isASAP
        {
         asap!(true)
        }
        else
        {
         
       
        self.scheduleTime!(self.dateAndTime)
            
        }
    }
    
    
    @IBAction func cancel(sender:UIButton){
        
        cancel?(true)
        
        
    }
    
}
