//
//  FavCuisineCollectionViewCell.swift
//  orderAround
//
//  Created by Chan Basha Shaik on 21/11/19.
//  Copyright © 2019 css. All rights reserved.
//

import UIKit

class FavCuisineCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var favImageView: UIImageView!
    
    @IBOutlet weak var lblSubCuisines: UILabel!
    @IBOutlet weak var lblCuisineName: UILabel!
    @IBOutlet weak var btnFav: UIButton!
    
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var labeloffer: UILabel!
    
    @IBOutlet weak var lblDeliveryTime: UILabel!
    
    @IBOutlet weak var labelRating: UILabel!
    
    //@IBOutlet weak var offerLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var offerImageHeight: NSLayoutConstraint!
    @IBOutlet weak var halalHeight: NSLayoutConstraint!
    @IBOutlet weak var imageOffer: UIImageView!
    
    @IBOutlet var closedLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        Common.setFont(to: lblCuisineName, isTitle: true, size: 14, fontType: .bold)
        Common.setFont(to: lblSubCuisines, isTitle: false, size: 13, fontType: .semiBold)
        Common.setFont(to: labeloffer, isTitle: true, size: 13, fontType: .semiBold)
        Common.setFont(to: labelRating, isTitle: false, size: 13, fontType: .semiBold)
        Common.setFont(to: lblRating, isTitle: true, size: 12, fontType: .semiBold)
        Common.setFont(to: closedLbl, isTitle: false, size: 13, fontType: .regular)
        lblSubCuisines.textColor = .gray
        closedLbl.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        closedLbl.text = APPLocalize.localizestring.closed.localize()
    }
    


}
