//
//  FavoriteModal.swift
//  orderAround
//
//  Created by Basha's MacBook Pro on 24/09/19.
//  Copyright © 2019 css. All rights reserved.
//

import Foundation


struct FavoriteModal : JSONSerializable {
    
    
    
  //  var name : String
    let id : Int?
    let name : String?
    let is_selected : Bool?
    
}
