//
//  RefundHistoryModel.swift
//  orderAround
//
//  Created by Prem's on 29/09/20.
//  Copyright © 2020 css. All rights reserved.
//

import Foundation

struct RefundHistoryModel: Codable{
    
    let refundHistories: [RefundHistoryData]?
    
    static func parse(data: Data) -> RefundHistoryModel? {
        do {
            let jsonDecoder = JSONDecoder()
            return try jsonDecoder.decode(RefundHistoryModel.self, from: data)
        } catch {
            return nil
        }
    }
}

struct RefundHistoryData: Codable {
    
   let id: Int?
   let transporterID: Int?
   let refundHistoryDescription: String?
   let note: String?
   let createdAt: String?
   let createdBy: String?
   let orderID, userID: Int?
   let type: String?
   let createdTo: String?
   let refundAmount: String?
   let order: Order?
   let orderDisputehelpID: Int?
   let disputeHelp: DisputeHelp?
   let status: String?
   let shopID: Int?
   let disputecomment: [Disputecomment]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case transporterID = "transporter_id"
        case refundHistoryDescription = "description"
        case note
        case createdAt = "created_at"
        case createdBy = "created_by"
        case orderID = "order_id"
        case userID = "user_id"
        case type
        case createdTo = "created_to"
        case refundAmount = "refund_amount"
        case order
        case orderDisputehelpID = "order_disputehelp_id"
        case disputeHelp = "dispute_help"
        case status
        case shopID = "shop_id"
        case disputecomment
    }
}



struct Order: Codable {
    let shiftID: Int?
    let ordertiming: [Ordertiming]?
    let createdAt: String?
    let id: Int?
    //let userDisputes: RefundHistory?
    let cancelledBy: String
    //let address: Address
    let orderReadyTime: Int?
    let cancelReasonID: Int?
    let isRated: Int?
    //let vehicles: Vehicles?
    let shopID, isUserRated: Int?
    let dispute: String?
    let transporterID: Int?
    let orderReadyStatus: Int?
    let invoiceID: String?
    let user: User?
    let eta: Int?
    let transporter: Transporter?
    let disputes: [Disputes]?
    let status: String?
    let userAddressID: Int?
    let routeKey: String?
    let orderOtp, userID: Int?
    let deliveryDate: String?
    let invoice: Invoice?
    //let reviewrating, reason: JSONNull?
    let scheduleStatus: Int?
    let transporterVehicleID: Int?
    let transporterRequest: String?
    //let shop: Shop?
    //let items: [Item]
    //let note: JSONNull?
    let pickupFromRestaurants: Int?

    enum CodingKeys: String, CodingKey {
        case shiftID = "shift_id"
        case ordertiming
        case createdAt = "created_at"
        case id
        //case userDisputes = "user_disputes"
        case cancelledBy = "cancelled_by"
        //case address
        case orderReadyTime = "order_ready_time"
        case cancelReasonID = "cancel_reason_id"
        case isRated = "is_rated"
        //case vehicles
        case shopID = "shop_id"
        case isUserRated = "is_user_rated"
        case dispute
        case transporterID = "transporter_id"
        case orderReadyStatus = "order_ready_status"
        case invoiceID = "invoice_id"
        case user, eta, transporter, disputes, status
        case userAddressID = "user_address_id"
        case routeKey = "route_key"
        case orderOtp = "order_otp"
        case userID = "user_id"
        case deliveryDate = "delivery_date"
        case invoice//, reviewrating, reason
        case scheduleStatus = "schedule_status"
        case transporterVehicleID = "transporter_vehicle_id"
        case transporterRequest = "transporter_request"
        //case shop, items, note
        case pickupFromRestaurants = "pickup_from_restaurants"
    }
}

